<?php

use Illuminate\Database\Seeder;
use App\Sport;

class SportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sport 		= new Sport();
        $sport->id 	= 1;
        $sport->name  = 'Soccer';
        $sport->save();

        $sport 		= new Sport();
        $sport->id 	= 16;
        $sport->name  = 'Baseball';
        $sport->save();


        $sport 		= new Sport();
        $sport->id 	= 17;
        $sport->name  = 'Ice Hockey';
        $sport->save();

        $sport 		= new Sport();
        $sport->id 	= 13;
        $sport->name  = 'Tennis';
        $sport->save();

        $sport 		= new Sport();
        $sport->id 	= 78;
        $sport->name  = 'Handball';
        $sport->save();
    }
}
