<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Admin
        $superUser 			  = new User();
        $superUser->first_name = 'Super Admin';
        $superUser->last_name  = 'Super Admin';
        $superUser->email      = 'a@a.com';
        $superUser->username   = 'super_admin';
        $superUser->password   = Hash::make('123456');
        $superUser->save();
        $superUser->assignRole('super-admin');



        // Admin
        $admin             = new User();
        $admin->first_name = 'Admin';
        $admin->last_name  = 'Admin';
        $admin->email      = 'b@b.com';
        $admin->username   = 'admin_admin';
        $admin->password   = Hash::make('123456');
        $admin->save();
        $admin->assignRole('admin');


        // Admin
        $cashier             = new User();
        $cashier->first_name = 'Cashier';
        $cashier->last_name  = 'Cashier';
        $cashier->email      = 'c@c.com';
        $cashier->username   = 'cashier_cashier';
        $cashier->password   = Hash::make('123456');
        $cashier->save();
        $cashier->assignRole('cashier');
        $admin->childrens()->save($cashier);

        // User
        $client 			     = new User();
        $client->first_name = 'User';
        $client->last_name  = 'User';
        $client->email 	  = 'u@u.com';
        $client->username   = 'user_user';
        $client->password   = Hash::make('123456');
        $client->save();
        $client->assignRole('client');
        $cashier->childrens()->save($client);


        $client                  = new User();
        $client->first_name = 'User 2';
        $client->last_name  = 'User 2';
        $client->email    = 'd@d.com';
        $client->username   = 'user2_user';
        $client->password   = Hash::make('123456');
        $client->save();
        $client->assignRole('client');
        $cashier->childrens()->save($client);

    }
}
