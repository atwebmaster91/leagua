<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$country = new Country();
		$country->cc = "ad";
		$country->name = "Andorra";
		$country->save();

		$country = new Country();
		$country->cc = "ae";
		$country->name = "United Arab Emirates";
		$country->save();

		$country = new Country();
		$country->cc = "af";
		$country->name = "Afghanistan";
		$country->save();

		$country = new Country();
		$country->cc = "ag";
		$country->name = "Antigua and Barbuda";
		$country->save();

		$country = new Country();
		$country->cc = "ai";
		$country->name = "Anguilla";
		$country->save();

		$country = new Country();
		$country->cc = "al";
		$country->name = "Albania";
		$country->save();

		$country = new Country();
		$country->cc = "am";
		$country->name = "Armenia";
		$country->save();

		$country = new Country();
		$country->cc = "ao";
		$country->name = "Angola";
		$country->save();

		$country = new Country();
		$country->cc = "aq";
		$country->name = "Antarctica";
		$country->save();

		$country = new Country();
		$country->cc = "ar";
		$country->name = "Argentina";
		$country->save();

		$country = new Country();
		$country->cc = "as";
		$country->name = "American Samoa";
		$country->save();

		$country = new Country();
		$country->cc = "at";
		$country->name = "Austria";
		$country->save();

		$country = new Country();
		$country->cc = "au";
		$country->name = "Australia";
		$country->save();

		$country = new Country();
		$country->cc = "aw";
		$country->name = "Aruba";
		$country->save();

		$country = new Country();
		$country->cc = "ax";
		$country->name = "Åland Islands";
		$country->save();

		$country = new Country();
		$country->cc = "az";
		$country->name = "Azerbaijan";
		$country->save();

		$country = new Country();
		$country->cc = "ba";
		$country->name = "Bosnia & Herzegovina";
		$country->save();

		$country = new Country();
		$country->cc = "bb";
		$country->name = "Barbados";
		$country->save();

		$country = new Country();
		$country->cc = "bd";
		$country->name = "Bangladesh";
		$country->save();

		$country = new Country();
		$country->cc = "be";
		$country->name = "Belgium";
		$country->save();

		$country = new Country();
		$country->cc = "bf";
		$country->name = "Burkina Faso";
		$country->save();

		$country = new Country();
		$country->cc = "bg";
		$country->name = "Bulgaria";
		$country->save();

		$country = new Country();
		$country->cc = "bh";
		$country->name = "Bahrain";
		$country->save();

		$country = new Country();
		$country->cc = "bi";
		$country->name = "Burundi";
		$country->save();

		$country = new Country();
		$country->cc = "bj";
		$country->name = "Benin";
		$country->save();

		$country = new Country();
		$country->cc = "bl";
		$country->name = "Saint Barthélemy";
		$country->save();

		$country = new Country();
		$country->cc = "bm";
		$country->name = "Bermuda";
		$country->save();

		$country = new Country();
		$country->cc = "en";
		$country->name = "England";
		$country->save();

		$country = new Country();
		$country->cc = "bo";
		$country->name = "Bolivia";
		$country->save();

		$country = new Country();
		$country->cc = "bq";
		$country->name = "Caribbean Netherlands";
		$country->save();

		$country = new Country();
		$country->cc = "br";
		$country->name = "Brazil";
		$country->save();

		$country = new Country();
		$country->cc = "bs";
		$country->name = "Bahamas";
		$country->save();

		$country = new Country();
		$country->cc = "bt";
		$country->name = "Bhutan";
		$country->save();

		$country = new Country();
		$country->cc = "bv";
		$country->name = "Bouvet Island";
		$country->save();

		$country = new Country();
		$country->cc = "bw";
		$country->name = "Botswana";
		$country->save();

		$country = new Country();
		$country->cc = "by";
		$country->name = "Belarus";
		$country->save();

		$country = new Country();
		$country->cc = "bz";
		$country->name = "Belize";
		$country->save();

		$country = new Country();
		$country->cc = "ca";
		$country->name = "Canada";
		$country->save();

		$country = new Country();
		$country->cc = "cc";
		$country->name = "Cocos (Keeling) Islands";
		$country->save();

		$country = new Country();
		$country->cc = "cd";
		$country->name = "Congo - Kinshasa";
		$country->save();

		$country = new Country();
		$country->cc = "cf";
		$country->name = "Central African Republic";
		$country->save();

		$country = new Country();
		$country->cc = "cg";
		$country->name = "Congo - Brazzaville";
		$country->save();

		$country = new Country();
		$country->cc = "ch";
		$country->name = "Switzerland";
		$country->save();

		$country = new Country();
		$country->cc = "ci";
		$country->name = "Côte d’Ivoire";
		$country->save();

		$country = new Country();
		$country->cc = "ck";
		$country->name = "Cook Islands";
		$country->save();

		$country = new Country();
		$country->cc = "cl";
		$country->name = "Chile";
		$country->save();

		$country = new Country();
		$country->cc = "cm";
		$country->name = "Cameroon";
		$country->save();

		$country = new Country();
		$country->cc = "cn";
		$country->name = "China";
		$country->save();

		$country = new Country();
		$country->cc = "co";
		$country->name = "Colombia";
		$country->save();

		$country = new Country();
		$country->cc = "cr";
		$country->name = "Costa Rica";
		$country->save();

		$country = new Country();
		$country->cc = "cu";
		$country->name = "Cuba";
		$country->save();

		$country = new Country();
		$country->cc = "cv";
		$country->name = "Cape Verde";
		$country->save();

		$country = new Country();
		$country->cc = "cw";
		$country->name = "Curaçao";
		$country->save();

		$country = new Country();
		$country->cc = "cx";
		$country->name = "Christmas Island";
		$country->save();

		$country = new Country();
		$country->cc = "cy";
		$country->name = "Cyprus";
		$country->save();

		$country = new Country();
		$country->cc = "cz";
		$country->name = "Czech";
		$country->save();

		$country = new Country();
		$country->cc = "de";
		$country->name = "Germany";
		$country->save();

		$country = new Country();
		$country->cc = "dj";
		$country->name = "Djibouti";
		$country->save();

		$country = new Country();
		$country->cc = "dk";
		$country->name = "Denmark";
		$country->save();

		$country = new Country();
		$country->cc = "dm";
		$country->name = "Dominica";
		$country->save();

		$country = new Country();
		$country->cc = "do";
		$country->name = "Dominican Republic";
		$country->save();

		$country = new Country();
		$country->cc = "dz";
		$country->name = "Algeria";
		$country->save();

		$country = new Country();
		$country->cc = "ec";
		$country->name = "Ecuador";
		$country->save();

		$country = new Country();
		$country->cc = "ee";
		$country->name = "Estonia";
		$country->save();

		$country = new Country();
		$country->cc = "eg";
		$country->name = "Egypt";
		$country->save();

		$country = new Country();
		$country->cc = "eh";
		$country->name = "Western Sahara";
		$country->save();

		$country = new Country();
		$country->cc = "er";
		$country->name = "Eritrea";
		$country->save();

		$country = new Country();
		$country->cc = "es";
		$country->name = "Spain";
		$country->save();

		$country = new Country();
		$country->cc = "et";
		$country->name = "Ethiopia";
		$country->save();

		$country = new Country();
		$country->cc = "fi";
		$country->name = "Finland";
		$country->save();

		$country = new Country();
		$country->cc = "fj";
		$country->name = "Fiji";
		$country->save();

		$country = new Country();
		$country->cc = "fk";
		$country->name = "Falkland Islands";
		$country->save();

		$country = new Country();
		$country->cc = "fm";
		$country->name = "Micronesia";
		$country->save();

		$country = new Country();
		$country->cc = "fo";
		$country->name = "Faroe Islands";
		$country->save();

		$country = new Country();
		$country->cc = "fr";
		$country->name = "France";
		$country->save();

		$country = new Country();
		$country->cc = "ga";
		$country->name = "Gabon";
		$country->save();

		$country = new Country();
		$country->cc = "gb";
		$country->name = "Great Britain";
		$country->save();

		$country = new Country();
		$country->cc = "gd";
		$country->name = "Grenada";
		$country->save();

		$country = new Country();
		$country->cc = "ge";
		$country->name = "Georgia";
		$country->save();

		$country = new Country();
		$country->cc = "gf";
		$country->name = "French Guiana";
		$country->save();

		$country = new Country();
		$country->cc = "gg";
		$country->name = "Guernsey";
		$country->save();

		$country = new Country();
		$country->cc = "gh";
		$country->name = "Ghana";
		$country->save();

		$country = new Country();
		$country->cc = "gi";
		$country->name = "Gibraltar";
		$country->save();

		$country = new Country();
		$country->cc = "gl";
		$country->name = "Greenland";
		$country->save();

		$country = new Country();
		$country->cc = "gm";
		$country->name = "Gambia";
		$country->save();

		$country = new Country();
		$country->cc = "gn";
		$country->name = "Guinea";
		$country->save();

		$country = new Country();
		$country->cc = "gp";
		$country->name = "Guadeloupe";
		$country->save();

		$country = new Country();
		$country->cc = "gq";
		$country->name = "Equatorial Guinea";
		$country->save();

		$country = new Country();
		$country->cc = "gr";
		$country->name = "Greece";
		$country->save();

		$country = new Country();
		$country->cc = "gs";
		$country->name = "South Georgia & South Sandwich Islands";
		$country->save();

		$country = new Country();
		$country->cc = "gt";
		$country->name = "Guatemala";
		$country->save();

		$country = new Country();
		$country->cc = "gu";
		$country->name = "Guam";
		$country->save();

		$country = new Country();
		$country->cc = "gw";
		$country->name = "Guinea-Bissau";
		$country->save();

		$country = new Country();
		$country->cc = "gy";
		$country->name = "Guyana";
		$country->save();

		$country = new Country();
		$country->cc = "hk";
		$country->name = "Hong Kong SAR China";
		$country->save();

		$country = new Country();
		$country->cc = "hm";
		$country->name = "Heard & McDonald Islands";
		$country->save();

		$country = new Country();
		$country->cc = "hn";
		$country->name = "Honduras";
		$country->save();

		$country = new Country();
		$country->cc = "hr";
		$country->name = "Croatia";
		$country->save();

		$country = new Country();
		$country->cc = "ht";
		$country->name = "Haiti";
		$country->save();

		$country = new Country();
		$country->cc = "hu";
		$country->name = "Hungary";
		$country->save();

		$country = new Country();
		$country->cc = "id";
		$country->name = "Indonesia";
		$country->save();

		$country = new Country();
		$country->cc = "ie";
		$country->name = "Ireland";
		$country->save();

		$country = new Country();
		$country->cc = "il";
		$country->name = "Israel";
		$country->save();

		$country = new Country();
		$country->cc = "im";
		$country->name = "Isle of Man";
		$country->save();

		$country = new Country();
		$country->cc = "in";
		$country->name = "India";
		$country->save();

		$country = new Country();
		$country->cc = "io";
		$country->name = "British Indian Ocean Territory";
		$country->save();

		$country = new Country();
		$country->cc = "iq";
		$country->name = "Iraq";
		$country->save();

		$country = new Country();
		$country->cc = "ir";
		$country->name = "Iran";
		$country->save();

		$country = new Country();
		$country->cc = "is";
		$country->name = "Iceland";
		$country->save();

		$country = new Country();
		$country->cc = "it";
		$country->name = "Italy";
		$country->save();

		$country = new Country();
		$country->cc = "je";
		$country->name = "Jersey";
		$country->save();

		$country = new Country();
		$country->cc = "jm";
		$country->name = "Jamaica";
		$country->save();

		$country = new Country();
		$country->cc = "jo";
		$country->name = "Jordan";
		$country->save();

		$country = new Country();
		$country->cc = "jp";
		$country->name = "Japan";
		$country->save();

		$country = new Country();
		$country->cc = "ke";
		$country->name = "Kenya";
		$country->save();

		$country = new Country();
		$country->cc = "kg";
		$country->name = "Kyrgyzstan";
		$country->save();

		$country = new Country();
		$country->cc = "kh";
		$country->name = "Cambodia";
		$country->save();

		$country = new Country();
		$country->cc = "ki";
		$country->name = "Kiribati";
		$country->save();

		$country = new Country();
		$country->cc = "km";
		$country->name = "Comoros";
		$country->save();

		$country = new Country();
		$country->cc = "kn";
		$country->name = "Saint Kitts and Nevis";
		$country->save();

		$country = new Country();
		$country->cc = "kp";
		$country->name = "North Korea";
		$country->save();

		$country = new Country();
		$country->cc = "kr";
		$country->name = "South Korea";
		$country->save();

		$country = new Country();
		$country->cc = "kw";
		$country->name = "Kuwait";
		$country->save();

		$country = new Country();
		$country->cc = "ky";
		$country->name = "Cayman Islands";
		$country->save();

		$country = new Country();
		$country->cc = "kz";
		$country->name = "Kazakhstan";
		$country->save();

		$country = new Country();
		$country->cc = "la";
		$country->name = "Laos";
		$country->save();

		$country = new Country();
		$country->cc = "lb";
		$country->name = "Lebanon";
		$country->save();

		$country = new Country();
		$country->cc = "lc";
		$country->name = "Saint Lucia";
		$country->save();

		$country = new Country();
		$country->cc = "li";
		$country->name = "Liechtenstein";
		$country->save();

		$country = new Country();
		$country->cc = "lk";
		$country->name = "Sri Lanka";
		$country->save();

		$country = new Country();
		$country->cc = "lr";
		$country->name = "Liberia";
		$country->save();

		$country = new Country();
		$country->cc = "ls";
		$country->name = "Lesotho";
		$country->save();

		$country = new Country();
		$country->cc = "lt";
		$country->name = "Lithuania";
		$country->save();

		$country = new Country();
		$country->cc = "lu";
		$country->name = "Luxembourg";
		$country->save();

		$country = new Country();
		$country->cc = "lv";
		$country->name = "Latvia";
		$country->save();

		$country = new Country();
		$country->cc = "ly";
		$country->name = "Libya";
		$country->save();

		$country = new Country();
		$country->cc = "ma";
		$country->name = "Morocco";
		$country->save();

		$country = new Country();
		$country->cc = "mc";
		$country->name = "Monaco";
		$country->save();

		$country = new Country();
		$country->cc = "md";
		$country->name = "Moldova";
		$country->save();

		$country = new Country();
		$country->cc = "me";
		$country->name = "Montenegro";
		$country->save();

		$country = new Country();
		$country->cc = "mf";
		$country->name = "Saint Martin";
		$country->save();

		$country = new Country();
		$country->cc = "mg";
		$country->name = "Madagascar";
		$country->save();

		$country = new Country();
		$country->cc = "mh";
		$country->name = "Marshall Islands";
		$country->save();

		$country = new Country();
		$country->cc = "mk";
		$country->name = "Macedonia";
		$country->save();

		$country = new Country();
		$country->cc = "ml";
		$country->name = "Mali";
		$country->save();

		$country = new Country();
		$country->cc = "mm";
		$country->name = "Myanmar (Burma)";
		$country->save();

		$country = new Country();
		$country->cc = "mn";
		$country->name = "Mongolia";
		$country->save();

		$country = new Country();
		$country->cc = "mo";
		$country->name = "Macau SAR China";
		$country->save();

		$country = new Country();
		$country->cc = "mp";
		$country->name = "Northern Mariana Islands";
		$country->save();

		$country = new Country();
		$country->cc = "mq";
		$country->name = "Martinique";
		$country->save();

		$country = new Country();
		$country->cc = "mr";
		$country->name = "Mauritania";
		$country->save();

		$country = new Country();
		$country->cc = "ms";
		$country->name = "Montserrat";
		$country->save();

		$country = new Country();
		$country->cc = "mt";
		$country->name = "Malta";
		$country->save();

		$country = new Country();
		$country->cc = "mu";
		$country->name = "Mauritius";
		$country->save();

		$country = new Country();
		$country->cc = "mv";
		$country->name = "Maldives";
		$country->save();

		$country = new Country();
		$country->cc = "mw";
		$country->name = "Malawi";
		$country->save();

		$country = new Country();
		$country->cc = "mx";
		$country->name = "Mexico";
		$country->save();

		$country = new Country();
		$country->cc = "my";
		$country->name = "Malaysia";
		$country->save();

		$country = new Country();
		$country->cc = "mz";
		$country->name = "Mozambique";
		$country->save();

		$country = new Country();
		$country->cc = "na";
		$country->name = "Namibia";
		$country->save();

		$country = new Country();
		$country->cc = "nc";
		$country->name = "New Caledonia";
		$country->save();

		$country = new Country();
		$country->cc = "ne";
		$country->name = "Niger";
		$country->save();

		$country = new Country();
		$country->cc = "nf";
		$country->name = "Norfolk Island";
		$country->save();

		$country = new Country();
		$country->cc = "ng";
		$country->name = "Nigeria";
		$country->save();

		$country = new Country();
		$country->cc = "ni";
		$country->name = "Nicaragua";
		$country->save();

		$country = new Country();
		$country->cc = "nl";
		$country->name = "Netherlands";
		$country->save();

		$country = new Country();
		$country->cc = "no";
		$country->name = "Norway";
		$country->save();

		$country = new Country();
		$country->cc = "np";
		$country->name = "Nepal";
		$country->save();

		$country = new Country();
		$country->cc = "nr";
		$country->name = "Nauru";
		$country->save();

		$country = new Country();
		$country->cc = "nu";
		$country->name = "Niue";
		$country->save();

		$country = new Country();
		$country->cc = "nz";
		$country->name = "New Zealand";
		$country->save();

		$country = new Country();
		$country->cc = "om";
		$country->name = "Oman";
		$country->save();

		$country = new Country();
		$country->cc = "pa";
		$country->name = "Panama";
		$country->save();

		$country = new Country();
		$country->cc = "pe";
		$country->name = "Peru";
		$country->save();

		$country = new Country();
		$country->cc = "pf";
		$country->name = "French Polynesia";
		$country->save();

		$country = new Country();
		$country->cc = "pg";
		$country->name = "Papua New Guinea";
		$country->save();

		$country = new Country();
		$country->cc = "ph";
		$country->name = "Philippines";
		$country->save();

		$country = new Country();
		$country->cc = "pk";
		$country->name = "Pakistan";
		$country->save();

		$country = new Country();
		$country->cc = "pl";
		$country->name = "Poland";
		$country->save();

		$country = new Country();
		$country->cc = "pm";
		$country->name = "Saint Pierre and Miquelon";
		$country->save();

		$country = new Country();
		$country->cc = "pn";
		$country->name = "Pitcairn Islands";
		$country->save();

		$country = new Country();
		$country->cc = "pr";
		$country->name = "Puerto Rico";
		$country->save();

		$country = new Country();
		$country->cc = "ps";
		$country->name = "Palestine";
		$country->save();

		$country = new Country();
		$country->cc = "pt";
		$country->name = "Portugal";
		$country->save();

		$country = new Country();
		$country->cc = "pw";
		$country->name = "Palau";
		$country->save();

		$country = new Country();
		$country->cc = "py";
		$country->name = "Paraguay";
		$country->save();

		$country = new Country();
		$country->cc = "qa";
		$country->name = "Qatar";
		$country->save();

		$country = new Country();
		$country->cc = "re";
		$country->name = "Réunion";
		$country->save();

		$country = new Country();
		$country->cc = "ro";
		$country->name = "Romania";
		$country->save();

		$country = new Country();
		$country->cc = "rs";
		$country->name = "Serbia";
		$country->save();

		$country = new Country();
		$country->cc = "ru";
		$country->name = "Russia";
		$country->save();

		$country = new Country();
		$country->cc = "rw";
		$country->name = "Rwanda";
		$country->save();

		$country = new Country();
		$country->cc = "sa";
		$country->name = "Saudi Arabia";
		$country->save();

		$country = new Country();
		$country->cc = "sb";
		$country->name = "Solomon Islands";
		$country->save();

		$country = new Country();
		$country->cc = "sc";
		$country->name = "Seychelles";
		$country->save();

		$country = new Country();
		$country->cc = "sd";
		$country->name = "Sudan";
		$country->save();

		$country = new Country();
		$country->cc = "se";
		$country->name = "Sweden";
		$country->save();

		$country = new Country();
		$country->cc = "sg";
		$country->name = "Singapore";
		$country->save();

		$country = new Country();
		$country->cc = "sh";
		$country->name = "Saint Helena";
		$country->save();

		$country = new Country();
		$country->cc = "si";
		$country->name = "Slovenia";
		$country->save();

		$country = new Country();
		$country->cc = "sj";
		$country->name = "Svalbard and Jan Mayen";
		$country->save();

		$country = new Country();
		$country->cc = "sk";
		$country->name = "Slovakia";
		$country->save();

		$country = new Country();
		$country->cc = "sl";
		$country->name = "Sierra Leone";
		$country->save();

		$country = new Country();
		$country->cc = "sm";
		$country->name = "San Marino";
		$country->save();

		$country = new Country();
		$country->cc = "sn";
		$country->name = "Senegal";
		$country->save();

		$country = new Country();
		$country->cc = "so";
		$country->name = "Somalia";
		$country->save();

		$country = new Country();
		$country->cc = "sr";
		$country->name = "Suriname";
		$country->save();

		$country = new Country();
		$country->cc = "ss";
		$country->name = "South Sudan";
		$country->save();

		$country = new Country();
		$country->cc = "st";
		$country->name = "São Tomé and Príncipe";
		$country->save();

		$country = new Country();
		$country->cc = "sv";
		$country->name = "El Salvador";
		$country->save();

		$country = new Country();
		$country->cc = "sx";
		$country->name = "Sint Maarten";
		$country->save();

		$country = new Country();
		$country->cc = "sy";
		$country->name = "Syria";
		$country->save();

		$country = new Country();
		$country->cc = "sz";
		$country->name = "Swaziland";
		$country->save();

		$country = new Country();
		$country->cc = "tc";
		$country->name = "Turks and Caicos Islands";
		$country->save();

		$country = new Country();
		$country->cc = "td";
		$country->name = "Chad";
		$country->save();

		$country = new Country();
		$country->cc = "tf";
		$country->name = "French Southern Territories";
		$country->save();

		$country = new Country();
		$country->cc = "tg";
		$country->name = "Togo";
		$country->save();

		$country = new Country();
		$country->cc = "th";
		$country->name = "Thailand";
		$country->save();

		$country = new Country();
		$country->cc = "tj";
		$country->name = "Tajikistan";
		$country->save();

		$country = new Country();
		$country->cc = "tk";
		$country->name = "Tokelau";
		$country->save();

		$country = new Country();
		$country->cc = "tl";
		$country->name = "Timor-Leste";
		$country->save();

		$country = new Country();
		$country->cc = "tm";
		$country->name = "Turkmenistan";
		$country->save();

		$country = new Country();
		$country->cc = "tn";
		$country->name = "Tunisia";
		$country->save();

		$country = new Country();
		$country->cc = "to";
		$country->name = "Tonga";
		$country->save();

		$country = new Country();
		$country->cc = "tr";
		$country->name = "Turkey";
		$country->save();

		$country = new Country();
		$country->cc = "tt";
		$country->name = "Trinidad and Tobago";
		$country->save();

		$country = new Country();
		$country->cc = "tv";
		$country->name = "Tuvalu";
		$country->save();

		$country = new Country();
		$country->cc = "tw";
		$country->name = "Taiwan";
		$country->save();

		$country = new Country();
		$country->cc = "tz";
		$country->name = "Tanzania";
		$country->save();

		$country = new Country();
		$country->cc = "ua";
		$country->name = "Ukraine";
		$country->save();

		$country = new Country();
		$country->cc = "ug";
		$country->name = "Uganda";
		$country->save();

		$country = new Country();
		$country->cc = "um";
		$country->name = "U.S. Outlying Islands";
		$country->save();

		$country = new Country();
		$country->cc = "us";
		$country->name = "USA";
		$country->save();

		$country = new Country();
		$country->cc = "uy";
		$country->name = "Uruguay";
		$country->save();

		$country = new Country();
		$country->cc = "uz";
		$country->name = "Uzbekistan";
		$country->save();

		$country = new Country();
		$country->cc = "va";
		$country->name = "Vatican City";
		$country->save();

		$country = new Country();
		$country->cc = "vc";
		$country->name = "St. Vincent & Grenadines";
		$country->save();

		$country = new Country();
		$country->cc = "ve";
		$country->name = "Venezuela";
		$country->save();

		$country = new Country();
		$country->cc = "vg";
		$country->name = "British Virgin Islands";
		$country->save();

		$country = new Country();
		$country->cc = "vi";
		$country->name = "U.S. Virgin Islands";
		$country->save();

		$country = new Country();
		$country->cc = "vn";
		$country->name = "Vietnam";
		$country->save();

		$country = new Country();
		$country->cc = "vu";
		$country->name = "Vanuatu";
		$country->save();

		$country = new Country();
		$country->cc = "wf";
		$country->name = "Wallis and Futuna";
		$country->save();

		$country = new Country();
		$country->cc = "ws";
		$country->name = "Samoa";
		$country->save();

		$country = new Country();
		$country->cc = "ye";
		$country->name = "Yemen";
		$country->save();

		$country = new Country();
		$country->cc = "yt";
		$country->name = "Mayotte";
		$country->save();

		$country = new Country();
		$country->cc = "za";
		$country->name = "South Africa";
		$country->save();

		$country = new Country();
		$country->cc = "zm";
		$country->name = "Zambia";
		$country->save();

		$country = new Country();
		$country->cc = "zw";
		$country->name = "Zimbabwe";
		$country->save();
    }
}
