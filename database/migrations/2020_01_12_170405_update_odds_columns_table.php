<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOddsColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('odds', function (Blueprint $table) {
            if(!Schema::hasColumn('odds','id')){
                $table->increments('id')->first();
            }
            if(!Schema::hasColumn('odds','updated_at')){
                $table->datetime('updated_at')->default(null)->after('created_at');
            }
            if(!Schema::hasColumn('odds','deleted_at')){
                $table->datetime('deleted_at')->default(null)->after('updated_at');
            }
            // if(!Schema::hasColumn('password_resets','deleted_at')){
            //     $table->dateTime('deleted_at')->default(null);
            // }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('odds', function (Blueprint $table) {
            // if(Schema::hasColumn('odds','updated_at')){
            //     $table->dropColumn('updated_at');
            // }
            if(Schema::hasColumn('odds','deleted_at')){
                $table->dropColumn('deleted_at');
            }

        });
    }
}
