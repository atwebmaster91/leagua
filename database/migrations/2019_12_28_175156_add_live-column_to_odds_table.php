<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLiveColumnToOddsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('odds', function (Blueprint $table) {
            //
            if(!Schema::hasColumn('odds','live_odds')){
                $table->boolean('live_odds')->nullable()->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('odds', function (Blueprint $table) {
            //
            if(Schema::hasColumn('odds','live_odds')){
                $table->dropColumn('live_odds');
            }
        });
    }
}
