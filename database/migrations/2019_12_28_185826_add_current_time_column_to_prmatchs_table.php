<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrentTimeColumnToPrmatchsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prematchs', function (Blueprint $table) {
            //
            if(!Schema::hasColumn('prematchs','live_time')){
                $table->string('live_time')->nullable()->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prematchs', function (Blueprint $table) {
            //
            if(Schema::hasColumn('prematchs','live_time')){
                $table->dropColumn('live_time');
            }
        });
    }
}
