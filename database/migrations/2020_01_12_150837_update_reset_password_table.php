<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateResetPasswordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('password_resets', function (Blueprint $table) {
            //
            if(!Schema::hasColumn('password_resets','id')){
                $table->increments('id')->first();
            }
            if(!Schema::hasColumn('password_resets','updated_at')){
                $table->dateTime('updated_at')->default(null);
            }
            // if(!Schema::hasColumn('password_resets','deleted_at')){
            //     $table->dateTime('deleted_at')->default(null);
            // }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('password_resets', function (Blueprint $table) {
            if(Schema::hasColumn('password_resets','id')){
                $table->dropColumn('id');
            }
            if(Schema::hasColumn('password_resets','updated_at')){
                $table->dropColumn('updated_at');
            }
            // if(Schema::hasColumn('password_resets','deleted_at')){
            //     $table->dropColumn('deleted_at');
            // }
        });
    }
}
