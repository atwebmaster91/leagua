<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateOddsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('odds', function (Blueprint $table) {
            //
            if(Schema::hasColumn('odds','odds_sub_categorie_id')){
                DB::statement('ALTER TABLE `odds` MODIFY `odds_sub_categorie_id` INTEGER UNSIGNED NULL;');
                //$table->integer('odds_sub_categorie_id')->nullable()->unsigned();
            }    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('odds', function (Blueprint $table) {
            //
            if(Schema::hasColumn('odds','odds_sub_categorie_id')){
                DB::statement('ALTER TABLE `odds` MODIFY `odds_sub_categorie_id` INTEGER UNSIGNED NOT NULL;');
                //$table->integer('odds_sub_categorie_id')->unsigned();
            }    
        });
    }
}
