<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteColumnsFromBetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bets', function (Blueprint $table) {
            //
            if(Schema::hasColumn('bets','prematch_id')){
                $table->dropForeign('bets_prematch_id_foreign');
                $table->dropColumn('prematch_id');
            }

            if(Schema::hasColumn('bets','odd_id')){
                $table->dropForeign('bets_odd_id_foreign');
                $table->dropColumn('odd_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bets', function (Blueprint $table) {
            //
            if(!Schema::hasColumn('bets','prematch_id')){
                
                $table->integer('prematch_id')->unsigned();

                $table->foreign('prematch_id')
                ->references('id')->on('prematchs')
                ->onDelete('cascade');
            }

            if(!Schema::hasColumn('bets','odd_id')){

                $table->integer('odd_id')->unsigned();
                
                $table->foreign('odd_id')
                ->references('id')->on('odds')
                ->onDelete('cascade');
            }
        });
    }
}
