<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOddsSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('odds_sub_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categorie_id')->unsigned();

            $table->string('name');
            $table->text('description');
            $table->timestamps();

            $table->foreign('categorie_id')
            ->references('id')->on('odds_categories')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('odds_sub_categories');
    }
}
