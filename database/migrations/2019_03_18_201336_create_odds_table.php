<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOddsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('odds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('odds_sub_categorie_id')->unsigned();
            $table->integer('prematch_id')->unsigned();

            $table->string('name')->nullable();
            $table->string('handicap')->nullable();
            $table->string('goals')->nullable();
            $table->string('opp')->nullable();
            $table->string('odds');
            $table->string('header')->nullable();

            $table->timestamps();

            $table->foreign('odds_sub_categorie_id')
            ->references('id')->on('odds_sub_categories')
            ->onDelete('cascade');

            $table->foreign('prematch_id')
            ->references('id')->on('prematchs')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('odds');
    }
}
