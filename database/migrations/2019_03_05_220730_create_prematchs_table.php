<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrematchsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prematchs', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->primary('id');

            $table->integer('league_id')->unsigned();
            $table->integer('home_id')->unsigned();
            $table->integer('away_id')->unsigned();

            $table->string('time');
            $table->string('time_status');

            $table->string('ss')->nullable();
            $table->integer('event_id');
            
            $table->tinyInteger('live')->default('0')->comment('0: NotLive; 1: live');

            $table->foreign('league_id')
                ->references('id')->on('leagues')
                ->onDelete('cascade');

            $table->foreign('home_id')
                ->references('id')->on('equipes')
                ->onDelete('cascade');

            $table->foreign('away_id')
                ->references('id')->on('equipes')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prematchs');
    }
}
