<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bets', function (Blueprint $table) {
            $table->increments('id');

            $table->float('amount')->nullable();
            $table->float('amount_to_win')->nullable();
            $table->boolean('status')->nullable();
            $table->integer('odd_id')->unsigned();
            $table->integer('prematch_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('odd_id')
            ->references('id')->on('odds')
            ->onDelete('cascade');

            $table->foreign('prematch_id')
            ->references('id')->on('prematchs')
            ->onDelete('cascade');

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bets');
    }
}
