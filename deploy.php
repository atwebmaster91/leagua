<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'leagua');

// Project repository
set('repository', 'https://gitlab.com/atwebmaster91/leagua.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);

//added by amine
set('git_tty', false);
set('ssh_multiplexing', false);

// Hosts

//host('project.com')
//    ->set('deploy_path', '~/{{application}}');    

host('167.71.91.210')
    ->user('root')
    ->identityFile('~/.ssh/leaguadeployerkey')
    ->set('deploy_path', '/var/www/html/leagua');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

//before('deploy:symlink', 'artisan:migrate');

