var FormValidation = function () {


    function isFloat(val) {
       // var floatRegex = /^-?\d+(?:[,|.]\d*?)?$/;
        var floatRegex = /^-?\d{0,8}(?:[.]\d{1,3}?)?$/;
        if (!floatRegex.test(val))
            return false;

        val = parseFloat(val);
        if (isNaN(val) || val < 0 )
            return false;
        return true;
    }


    $.validator.addMethod("float", function(value, element) { 
        return isFloat(value)
    }, "Veuillez saisir un float."); 


    $.validator.addMethod("equals", function(value, element, param) {
        // Check if the value is in the array.
        return this.optional(element) || $.inArray(value, param) > -1; 
    }, "Veuillez saisir une valeur correct."); 


    var login = function() {
        var form = $('#login');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: true, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    email: {
                        required: true
                    },
                    password: {
                        minlength: 6,
                        required: true
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success.hide();
                    error.show();
                    $(".alertadd").hide();
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont.length > 0) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    $(".alertadd").hide();
                    form.submit();
                }
            });
    }


    var reset = function() {
        var form = $('#reset');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: true, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    email: {
                        required: true,
                        email: true,
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success.hide();
                    error.show();
                    $(".alertadd").hide();
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont.length > 0) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    $(".alertadd").hide();
                    form.submit();
                }
            });
    }


    var account = function() {
        var form = $('#admin_account');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        form.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                avatar: {
                  extension: "png|jpg|jpeg"
                },
                first_name: {
                    required: true,
                    minlength: 2
                },
                last_name: {
                    required: true,
                    minlength: 2  
                },
                email: {
                    required: true,
                    email: true,
                    remote: { 
                        url:"check_email", 
                        data: {
                            "id_admin": form.data('id')
                        }
                    }
                },
                username: {
                    minlength:2,
                    required: true,
                    remote: { 
                        url:"check_username", 
                        data: {
                            "id_admin": form.data('id')
                        }
                    }
                },
                password: {
                    minlength: 6
                },
                password_confirmation: {
                    equalTo: "#password"
                },
            },
            messages: { // custom messages for radio buttons and checkboxes
                email:{
                    remote: jQuery.validator.format('Ce Email est déjà utilisé.')
                },
                username:{
                    remote: jQuery.validator.format('Ce Username est déjà utilisé.')
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                $(".alertadd").hide();
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.length > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
                if (element.attr('type') ==  "file") {
                    error.appendTo(element.closest('.form-group'));
                } 
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success.show();
                error.hide();
                $(".alertadd").hide();
                form.submit();
            }
        });

        $('input[name="avatar"]').on('change', function(){ 
            $(this).valid();
        });
    }


    var admin_edit = function() {
        var form = $('#admin_edit');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        form.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                avatar: {
                  extension: "png|jpg|jpeg"
                },
                first_name: {
                    required: true,
                    minlength: 2
                },
                last_name: {
                    required: true,
                    minlength: 2  
                },
                email: {
                    required: true,
                    email: true,
                    remote: { 
                        url:"../../check_email", 
                        data: {
                            "id_admin": form.data('id')
                        }
                    }
                },
                password: {
                    minlength: 6
                },
                password_confirmation: {
                    equalTo: "#password"
                },
            },
            messages: { // custom messages for radio buttons and checkboxes
                email:{
                    remote: jQuery.validator.format('Ce Email est déjà utilisé.')
                },
                username:{
                    remote: jQuery.validator.format('Ce Username est déjà utilisé.')
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                $(".alertadd").hide();
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.length > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
                if (element.attr('type') ==  "file") {
                    error.appendTo(element.closest('.form-group'));
                } 
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success.show();
                error.hide();
                $(".alertadd").hide();
                form.submit();
            }
        });

        $('input[name="avatar"]').on('change', function(){ 
            $(this).valid();
        });
    }

    var admin_create = function() {
        var form = $('#admin_create');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        form.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                avatar: {
                  extension: "png|jpg|jpeg"
                },
                first_name: {
                    required: true,
                    minlength: 2
                },
                last_name: {
                    required: true,
                    minlength: 2  
                },
                email: {
                    required: true,
                    email: true,
                    remote: { 
                        url:"../check_email", 
                        data: {
                            "id_admin": null
                        }
                    }
                },
                username: {
                    minlength:2,
                    required: true,
                    remote: { 
                        url:"../check_username", 
                        data: {
                            "id_admin": null
                        }
                    }
                },
                password: {
                    minlength: 6
                },
                password_confirmation: {
                    equalTo: "#password"
                },
            },
            messages: { // custom messages for radio buttons and checkboxes
                email:{
                    remote: jQuery.validator.format('Ce Email est déjà utilisé.')
                },
                username:{
                    remote: jQuery.validator.format('Ce Username est déjà utilisé.')
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                $(".alertadd").hide();
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.length > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
                if (element.attr('type') ==  "file") {
                    error.appendTo(element.closest('.form-group'));
                } 
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success.show();
                error.hide();
                $(".alertadd").hide();
                form.submit();
            }
        });

        $('input[name="avatar"]').on('change', function(){ 
            $(this).valid();
        });
    }

    var cashier_edit = function() {
        var form = $('#cashier_edit');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        form.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                avatar: {
                  extension: "png|jpg|jpeg"
                },
                first_name: {
                    required: true,
                    minlength: 2
                },
                last_name: {
                    required: true,
                    minlength: 2  
                },
                email: {
                    required: true,
                    email: true,
                    remote: { 
                        url:"../../check_email", 
                        data: {
                            "id_admin": form.data('id')
                        }
                    }
                },
                password: {
                    minlength: 6
                },
                password_confirmation: {
                    equalTo: "#password"
                },
                parent_id: {
                    required: true
                }
            },
            messages: { // custom messages for radio buttons and checkboxes
                email:{
                    remote: jQuery.validator.format('Ce Email est déjà utilisé.')
                },
                username:{
                    remote: jQuery.validator.format('Ce Username est déjà utilisé.')
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                $(".alertadd").hide();
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.length > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }

                if (element.is('select')) {
                    error.insertAfter(element.closest('.form-group').find('.select2-container'));
                }

                if (element.attr('type') ==  "file") {
                    error.appendTo(element.closest('.form-group'));
                } 
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success.show();
                error.hide();
                $(".alertadd").hide();
                form.submit();
            }
        });

        $('input[name="avatar"]').on('change', function(){ 
            $(this).valid();
        });

        $("select").on("select2:close", function (e) {  
            $(this).valid(); 
        });
    }

    var cashier_create = function() {
        var form = $('#cashier_create');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        form.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                avatar: {
                  extension: "png|jpg|jpeg"
                },
                first_name: {
                    required: true,
                    minlength: 2
                },
                last_name: {
                    required: true,
                    minlength: 2  
                },
                email: {
                    required: true,
                    email: true,
                    remote: { 
                        url:"../check_email", 
                        data: {
                            "id_admin": null
                        }
                    }
                },
                username: {
                    minlength: 2,
                    required: true,
                    remote: { 
                        url:"../check_username", 
                        data: {
                            "id_admin": null
                        }
                    }
                },
                password: {
                    minlength: 6
                },
                password_confirmation: {
                    equalTo: "#password"
                },
                parent_id: {
                    required: true
                }

            },
            messages: { // custom messages for radio buttons and checkboxes
                email:{
                    remote: jQuery.validator.format('Ce Email est déjà utilisé.')
                },
                username:{
                    remote: jQuery.validator.format('Ce Username est déjà utilisé.')
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                $(".alertadd").hide();
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.length > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
                
                if (element.is('select')) {
                    error.insertAfter(element.closest('.form-group').find('.select2-container'));
                }

                if (element.attr('type') ==  "file") {
                    error.appendTo(element.closest('.form-group'));
                } 
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success.show();
                error.hide();
                $(".alertadd").hide();
                form.submit();
            }
        });

        $('input[name="avatar"]').on('change', function(){ 
            $(this).valid();
        });

        $("select").on("select2:close", function (e) {  
            $(this).valid(); 
        });
    }


    var money = function() {
        var form = $('#money');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        form.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                amount: {
                    required: true,
                    float: true
                },
                type: {
                    required: true,
                    equals: ['0', '1']
                },
                receiver_id: {
                    required: true
                }

            },
            messages: { // custom messages for radio buttons and checkboxes
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                $(".alertadd").hide();
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.length > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
                
                if (element.is('select')) {
                    error.insertAfter(element.closest('.form-group').find('.select2-container'));
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success.show();
                error.hide();
                $(".alertadd").hide();
                form.submit();
            }
        });

        $("select").on("select2:close", function (e) {  
            $(this).valid(); 
        });
    }
    return {

        init: function () {
            login()
            reset()
            account()
            admin_edit()
            admin_create()
            cashier_edit()
            cashier_create()
            money()
        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
});