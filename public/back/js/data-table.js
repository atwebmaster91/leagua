$(function() {
    var handleBootstrapSwitch = function() {
        $('.tooltips').tooltip();
        $('.make-switch').bootstrapSwitch().on("switchChange.bootstrapSwitch", function (event, state) {
            var switcher = $(this);
            var pathname = window.location.pathname;
            var switch_for = $(this).data('for');
            var statut = switcher.is('checked');
            $.ajax({
                url: pathname+'/status',
                data: {
                    id: switcher.data('id'),
                    _token : $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                datatype: "json",
                success: function (data) {
                    if ((data.status  && data.status == true) || data == true) {
                        $.toast({
                            heading: 'Modification du statut',
                            text: 'A été faite avec succés.',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'success',
                            hideAfter: 3500, 
                            stack: 6
                          });
                    }else{
                        switcher.bootstrapSwitch('state', !statut , true);
                        $.toast({
                            heading: 'Modification du statut',
                            text: 'Une erreur est intervenue.',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'warning',
                            hideAfter: 3500, 
                            stack: 6
                        });
                    }
                },
                error: function () {
                    switcher.bootstrapSwitch('state', !statut , true);
                    $.toast({
                        heading: 'Modification du statut',
                        text: 'Une erreur est intervenue.',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            });
        });
    };

    if ($('#admins-table').length) {
        window.datatable = $('#admins-table').DataTable({
            processing: true,
            serverSide: true,
            deferRender: true,
            order: [[ 6, "desc" ]],
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.10/i18n/French.json"
            },
            ajax: {
                "url": "admins/datatable",
                "type": "POST",
                "data" : {
                    "_token" : $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'avatar', name: 'avatar' , orderable: false, searchable: false },
                { data: 'first_name', name: 'first_name' },
                { data: 'last_name', name: 'last_name' },
                { data: 'email', name: 'email' },
                { data: 'money', name: 'money' },
                { data: 'status', name: 'status', orderable: false, searchable: false },
                { data: 'created_at', name: 'created_at', type: 'date-euro' },
                { data: 'actions', name: 'actions', orderable: false, searchable: false }
            ],
            drawCallback: handleBootstrapSwitch
        });
    }
    
    if ($('#cashiers-table').length) {

        var roles = $('#cashiers-table').data('user');
        hideParent = roles.includes("admin");

        window.datatable = $('#cashiers-table').DataTable({
            processing: true,
            serverSide: true,
            deferRender: true,
            order: [[ 7, "desc" ]],
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.10/i18n/French.json"
            },
            ajax: {
                "url": "cashiers/datatable",
                "type": "POST",
                "data" : {
                    "_token" : $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'avatar', name: 'avatar' , orderable: false, searchable: false },
                { data: 'first_name', name: 'first_name' },
                { data: 'last_name', name: 'last_name' },
                { data: 'parent', name: 'parent' },
                { data: 'email', name: 'email' },
                { data: 'money', name: 'money' },
                { data: 'status', name: 'status', orderable: false, searchable: false },
                { data: 'created_at', name: 'created_at', type: 'date-euro' },
                { data: 'actions', name: 'actions', orderable: false, searchable: false }
            ],
            drawCallback: handleBootstrapSwitch
        });

        window.datatable.column(3).visible(!hideParent);
    }

    if ($('#clients-table').length) {

        var roles = $('#clients-table').data('user');
        hideParent = roles.includes("cashier");
        window.datatable = $('#clients-table').DataTable({
            processing: true,
            serverSide: true,
            deferRender: true,
            order: [[ 7, "desc" ]],
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.10/i18n/French.json"
            },
            ajax: {
                "url": "clients/datatable",
                "type": "POST",
                "data" : {
                    "_token" : $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'avatar', name: 'avatar' , orderable: false, searchable: false },
                { data: 'first_name', name: 'first_name' },
                { data: 'last_name', name: 'last_name' },
                { data: 'parent', name: 'parent' },
                { data: 'email', name: 'email' },
                { data: 'money', name: 'money' },
                { data: 'status', name: 'status', orderable: false, searchable: false },
                { data: 'created_at', name: 'created_at', type: 'date-euro' },
                { data: 'actions', name: 'actions', orderable: false, searchable: false }
            ],
            drawCallback: handleBootstrapSwitch
        });

        window.datatable.column(3).visible(!hideParent);
    }

    if ($('#transactions').length) {

        window.datatable = $('#transactions').DataTable({
            processing: true,
            serverSide: true,
            deferRender: true,
            order: [[ 4, "desc" ]],
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.10/i18n/French.json"
            },
            ajax: {
                "url": "transactions/datatable",
                "type": "POST",
                "data" : {
                    "_token" : $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'sender_id', name: 'sender_id' },
                { data: 'receiver_id', name: 'receiver_id' },
                { data: 'amount', name: 'amount' },
                { data: 'type', name: 'type' },
                { data: 'created_at', name: 'created_at', type: 'date-euro' }
            ],
            drawCallback: handleBootstrapSwitch
        });
    }

}); 