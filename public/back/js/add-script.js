
function remove(table,id){

    var pathname = window.location.pathname;
    swal({
            title: "Êtes-vous sûr?",
            text: "Vous êtes entrain de supprimer !",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Oui, supprimer !",
            cancelButtonText: "Non, annuler !",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: pathname+'/delete',
                    data: {
                        id:id,
                        _token : $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    datatype: "json",
                    success: function (data) {

                        if(table == 'contacts'){
                            if(data == true){
                                swal({ title: "Supprimer!", text: "L'enregistrement a éte supprimé.", type:"success"},
                                function(isConfirm) {
                                    if (isConfirm) {
                                        setTimeout(location.reload.bind(location), 300);  
                                    }
                                });
                            }else{
                                swal({ title: "Erreur!", text: "Une erreur est intervenue.", type:"error"},
                                function(isConfirm) {
                                    if (isConfirm) {
                                        setTimeout(location.reload.bind(location), 300);  
                                    }
                                });
                            }
                        }else{
                            if(data == true){
                                swal("Supprimer!", "L'enregistrement a éte supprimé.", "success");
                                window.datatable.ajax.reload();    
                            }else{
                                swal("Erreur!", "Une erreur est intervenue.", "error");
                                window.datatable.ajax.reload();
                            }
                        }
                    },
                    error: function (data) {
                        swal("Annuler", "Une erreur est intervenue dans le script !", "error");
                    }
                });

            } else {
                swal("Annuler", " Votre est sécurisé :)", "error");
            }
        });
}

jQuery(document).ready(function() {
    if($('.select2').length > 0){
        $(".select2").select2({
            allowClear: true,
            placeholder: $(this).data('placeholder'),
        });
    }
    
});