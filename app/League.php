<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class League extends Model
{
	protected $fillable = ['id','sport_id','countrie_cc','name'];
    protected $primaryKey = 'id';
    public $incrementing = false;

    public function sport(){
    	return $this->belongsTo(Sport::class);
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function prematchs(){
    	return $this->hasMany(Prematch::class)->where('live',0);
    }
    
    public function prematchsLive(){
        return $this->hasMany(Prematch::class)->where('live',1);
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }
}
