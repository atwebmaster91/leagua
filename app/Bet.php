<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Prematch;
use App\Odd;

class Bet extends Model
{
    //
    protected $fillable = ['amount', 'played_amount', 'amount_to_win','status','prematch_id','odd_id','user_id','created_at','updated_at'];

    protected $hidden = [];

    public function user(){
    	return $this->belongsTo(User::class,'user_id');
    }

    public function odds(){
        return $this->belongsToMany('App\Odd');
    }

}
