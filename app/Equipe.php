<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipe extends Model
{
	protected $fillable = ['id','sport_id','name'];
    protected $primaryKey = 'id';
    public $incrementing = false;
    

    public function sport(){
    	return $this->belongsTo(Sport::class);
    }
}
