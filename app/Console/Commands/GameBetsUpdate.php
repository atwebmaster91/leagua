<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Bet;
use App\Odd;
use App\Prematch;
use Illuminate\Http\Request;
use App\Jobs\ProcessPodcast;
use GuzzleHttp\Client;
use Log;

class GameBetsUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bets:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check all bets and update them';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client();
        $this->accessToken =  env('BETSAPI_TOKEN', null);
        $this->api_url = env('BETSAPI_API_URL', null);
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        // Finding a random word
        $users = User::with(['bets' => function($lives) {
            $lives->where('bets.status', null);
      },'bets.odds','bets.odds.sub_categorie'])->get();

        foreach ($users as $user) {

            if($user->bets){
                
                foreach($user->bets as $bet){


                    $betStatus = true;
                    foreach($bet->odds as $odd){
                        $db_odd = Odd::where('id',$odd->id)->with(['prematch'])->first();
                        //get result
                        if($db_odd){
                            $game_result = $this->api_url . "/result";
                            $data['token'] = $this->accessToken;
                            $data['event_id'] = $db_odd->prematch->id;
                            $response = $this->client->get($game_result, ['query' => $data]);
                            $game = json_decode($response->getBody(true)->getContents(),true);

                            $gameResult = $game['results'][0];

                            //get the prematch and update the live status and time
                            if($gameResult['time_status'] == 3){
                                $bd_prematch = Prematch::where('id',$db_odd->prematch->id)->first();
                                $bd_prematch->time_status == $gameResult['time_status'];
                                $bd_prematch->live = 0;
                                $bd_prematch->live_time = null;
                                $bd_prematch->save();
                            }
                            
                            
                            //get the results in numbers
                            $goalsArray = explode('-',$gameResult['ss']);
                            $homeGoalsNbr = floatval($goalsArray[0]);
                            $awayGoalsNbr = floatval($goalsArray[1]);
                            //                            
                            if($db_odd->odds_sub_categorie_id){

                                $homeGoalsNbr = floatval($gameResult['scores'][2]['home']);
                                $awayGoalsNbr = floatval($gameResult['scores'][2]['away']);

                                switch ($db_odd->odds_sub_categorie_id) {
                                    case 2://goal_line
                                    case 4://alternative_goal_line
                                    case 9://goals_over_under
                                    case 10://alternative_total_goals
                                        switch ($db_odd->header){
                                            case 'Over':
                                                if(floatval($db_odd->name) && ($homeGoalsNbr + $awayGoalsNbr) < floatval($db_odd->name)){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 3;
                                                }
                                            break;
                                            case 'Under':
                                                if(floatval($db_odd->name) && ($homeGoalsNbr + $awayGoalsNbr) > floatval($db_odd->name)){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 3;
                                                }
                                            break;
                                        }
                                        break;
                                    case 6://1st_half_goal_line
                                    case 8://alternative_1st_half_goal_line
                                    case 20://first_half_goals
                                        //get goals number of first half
                                        $firstHalfgoalsNbr = floatval($gameResult['scores'][1]['home']) + floatval($gameResult['scores'][1]['away']);
                                        switch ($db_odd->header){
                                            case 'Over':
                                                if(floatval($db_odd->name) && ($firstHalfgoalsNbr < floatval($db_odd->name))){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 3;
                                                }
                                            break;
                                            case 'Under':
                                                if(floatval($db_odd->name) && ($firstHalfgoalsNbr > floatval($db_odd->name))){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 3;
                                                }
                                            break;
                                        }
                                    break;
                                    case 11://result_total_goals
                                        if(strpos($db_odd->name,$gameResult['home']['name'])){
                                            if(strpos($db_odd->name,'Over')){
                                                if($homeGoalsNbr < $awayGoalsNbr && ($homeGoalsNbr + $awayGoalsNbr < floatval($db_odd->handicap))){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 2;
                                                }
                                            } elseif(strpos($db_odd->name,'Under')){
                                                if($homeGoalsNbr < $awayGoalsNbr && ($homeGoalsNbr + $awayGoalsNbr > floatval($db_odd->handicap))){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 2;
                                                }
                                            }

                                        } elseif(strpos($db_odd->name,$gameResult['away']['name'])){
                                            if(strpos($db_odd->name,'Over')){
                                                if($homeGoalsNbr > $awayGoalsNbr && ($homeGoalsNbr + $awayGoalsNbr < floatval($db_odd->handicap))){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 2;
                                                }
                                            } elseif(strpos($db_odd->name,'Under')){
                                                if($homeGoalsNbr > $awayGoalsNbr && ($homeGoalsNbr + $awayGoalsNbr > floatval($db_odd->handicap))){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 2;
                                                }
                                            }
                                        }
                                    break;
                                    case 12://total_goals_both_teams_to_score
                                        
                                        $goalsRequired = preg_replace('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', '', $db_odd->name);
                                        $goalsRequired = floatval($goalsRequired);

                                        if(strpos($db_odd->name,'Yes')){

                                            if(strpos($db_odd->name,'Over')){
                                                
                                                if($homeGoalsNbr < 1 && $awayGoalsNbr < 1 && ($homeGoalsNbr + $awayGoalsNbr < $goalsRequired)){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 2;
                                                }
                                            } elseif(strpos($db_odd->name,'Under')){
                                                if($homeGoalsNbr < 1 && $awayGoalsNbr < 1 && ($homeGoalsNbr + $awayGoalsNbr > $goalsRequired)){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 2;
                                                }
                                            }

                                        } elseif(strpos($db_odd->name,'No')){
                                            if(strpos($db_odd->name,'Over')){
                                                if($homeGoalsNbr > 1 || $awayGoalsNbr > 1 && ($homeGoalsNbr + $awayGoalsNbr < $goalsRequired)){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 2;
                                                }
                                            } elseif(strpos($db_odd->name,'Under')){
                                                if($homeGoalsNbr > 1 || $awayGoalsNbr > 1 && ($homeGoalsNbr + $awayGoalsNbr > $goalsRequired)){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 2;
                                                }
                                            }
                                        }
                                    break;
                                    case 14://number_of_goals_in_match

                                        if(strpos($db_odd->name,'Over')){

                                            $goalsRequired = preg_replace('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', '', $db_odd->name);
                                            $goalsRequired = floatval($goalsRequired);

                                            if($homeGoalsNbr + $awayGoalsNbr < $goalsRequired){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }

                                        } elseif(strpos($db_odd->name,'Under')){

                                            $goalsRequired = preg_replace('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', '', $db_odd->name);
                                            $goalsRequired = floatval($goalsRequired);
                                            
                                            if($homeGoalsNbr + $awayGoalsNbr < $goalsRequired){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                        } else {
                                            $goals = array();
                                            preg_match_all('!\d+!', $db_odd->name, $goals);
                                            $exactNbrofGoals = $homeGoalsNbr + $awayGoalsNbr;
                                            if(!in_array($goals,$exactNbrofGoals)){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                        }
                                    break;
                                    case 15://both_teams_to_score
                                        switch ($db_odd->name){
                                            case 'Yes':
                                                if($homeGoalsNbr < 1 || $awayGoalsNbr < 1){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 3;
                                                }
                                            break;
                                            case 'No':
                                                if($homeGoalsNbr > 1 && $awayGoalsNbr > 1){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 3;
                                                }
                                            break;
                                        }
                                    break;
                                    case 16://teams_to_score
                                        if(strpos($db_odd->name,'Both')){
                                            if($homeGoalsNbr < 1 || $awayGoalsNbr < 1){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                        } elseif(strpos($db_odd->name,$gameResult['home']['name'])){
                                            if($homeGoalsNbr < 1 && $awayGoalsNbr > 1){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                        } elseif(strpos($db_odd->name,$gameResult['away']['name'])){
                                            if($homeGoalsNbr > 1 && $awayGoalsNbr < 1){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                        } elseif(strpos($db_odd->name,'No Goal')){
                                            if($homeGoalsNbr > 1 || $awayGoalsNbr > 1){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                        }
                                    break;
                                    case 17://both_teams_to_score_in_1st_half
                                        $firstHalfHomeGoalsNbr = floatval($gameResult['scores'][1]['home']);
                                        $awaytHalfHomeGoalsNbr = floatval($gameResult['scores'][1]['away']);
                                        switch ($db_odd->name){
                                            case 'Yes':
                                                if($firstHalfHomeGoalsNbr < 1 || $awaytHalfHomeGoalsNbr < 1){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 3;
                                                }
                                            break;
                                            case 'No':
                                                if($firstHalfHomeGoalsNbr > 1 && $awaytHalfHomeGoalsNbr > 1){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 3;
                                                }
                                            break;
                                        }
                                    break;
                                    case 18://both_teams_to_score_in_2nd_half
                                        $firstHalfHomeGoalsNbr = floatval($gameResult['scores'][1]['home']);
                                        $firstHalfAwayGoalsNbr = floatval($gameResult['scores'][1]['away']);

                                        $secondHalfHomeGoalsNbr = $homeGoalsNbr - $firstHalfHomeGoalsNbr;
                                        $secondHalfAwayGoalsNbr = $awayGoalsNbr - $firstHalfAwayGoalsNbr;

                                        switch ($db_odd->name){
                                            case 'Yes':
                                                if($secondHalfHomeGoalsNbr < 1 || $secondHalfAwayGoalsNbr < 1){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 3;
                                                }
                                            break;
                                            case 'No':
                                                if($secondHalfHomeGoalsNbr > 1 && $secondHalfAwayGoalsNbr > 1){
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 3;
                                                }
                                            break;
                                        }
                                    break;
                                    case 19://both_teams_to_score_1st_half_2nd_half

                                        $values = explode('/',$db_odd->name);
                                        $first_half = $values[0];
                                        $second_half = $values[1];

                                        $firstHalfHomeGoalsNbr = floatval($gameResult['scores'][1]['home']);
                                        $firstHalfAwayGoalsNbr = floatval($gameResult['scores'][1]['away']);

                                        $secondHalfHomeGoalsNbr = $homeGoalsNbr - $firstHalfHomeGoalsNbr;
                                        $secondHalfAwayGoalsNbr = $awayGoalsNbr - $firstHalfAwayGoalsNbr;

                                        switch ($first_half){
                                            case 'Yes':
                                                if($firstHalfHomeGoalsNbr > 0 && $firstHalfAwayGoalsNbr > 0){
                                                    switch ($second_half){
                                                        case 'Yes':
                                                            if($secondHalfHomeGoalsNbr < 1 || $secondHalfAwayGoalsNbr < 1){
                                                                $betStatus = false;
                                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                                $loosingBet->status = 0;
                                                                $loosingBet->save();
                                                                break 4;
                                                            }
                                                        break;
                                                        case 'No':
                                                            if($secondHalfHomeGoalsNbr > 1 && $secondHalfAwayGoalsNbr > 1){
                                                                $betStatus = false;
                                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                                $loosingBet->status = 0;
                                                                $loosingBet->save();
                                                                break 4;
                                                            }
                                                        break;
                                                    }
                                                } else {
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 3;
                                                }
                                            break;
                                            case 'No':
                                                if($firstHalfHomeGoalsNbr < 1 || $firstHalfAwayGoalsNbr < 1){
                                                    switch ($second_half){
                                                        case 'Yes':
                                                            if($secondHalfHomeGoalsNbr < 1 || $secondHalfAwayGoalsNbr < 1){
                                                                $betStatus = false;
                                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                                $loosingBet->status = 0;
                                                                $loosingBet->save();
                                                                break 4;
                                                            }
                                                        break;
                                                        case 'No':
                                                            if($secondHalfHomeGoalsNbr > 1 && $secondHalfAwayGoalsNbr > 1){
                                                                $betStatus = false;
                                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                                $loosingBet->status = 0;
                                                                $loosingBet->save();
                                                                break 4;
                                                            }
                                                        break;
                                                    }
                                                } elseif($firstHalfHomeGoalsNbr > 0 && $firstHalfAwayGoalsNbr > 0) {
                                                    $betStatus = false;
                                                    $loosingBet = Bet::where('id',$bet->id)->first();
                                                    $loosingBet->status = 0;
                                                    $loosingBet->save();
                                                    break 3;
                                                }
                                            break;
                                        }
                                    break;
                                    case 21://exact_1st_half_goals
                                        //get goals number of first half
                                        $goalsRequired = preg_replace('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', '', $db_odd->name);
                                        $goalsRequired = floatval($goalsRequired);

                                        $firstHalfgoalsNbr = floatval($gameResult['scores'][1]['home']) + floatval($gameResult['scores'][1]['away']);
                                        
                                        if(strpos($db_odd->name,'+')){
                                            
                                            if($firstHalfgoalsNbr < $goalsRequired){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }

                                        } else {

                                            if($firstHalfgoalsNbr != $goalsRequired){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                        }
                                    break;
                                    case 23://first_team_to_score
                            
                                        $firstGoalEvent = null;

                                        foreach($gameResult['events'] as $event){
                                            if(strpos($event['text'],'1st Goal')){
                                                $firstGoalEvent = $event['text'];
                                                break;
                                            }
                                        }

                                        if(strpos($db_odd->name,$gameResult['home']['name'])){
                                            
                                            if($firstGoalEvent == null || $firstGoalEvent && !strpos($firstGoalEvent,$gameResult['home']['name'])){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }

                                        } elseif(strpos($db_odd->name,$gameResult['away']['name'])){

                                            if($firstGoalEvent == null || $firstGoalEvent && !strpos($firstGoalEvent,$gameResult['away']['name'])){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                        } elseif(strpos($db_odd->name,'No Goals')){

                                            if($firstGoalEvent){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                        }
                                    break;
                                    case 24://early_goal
                                        
                                        $betGoalTime = preg_replace('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', '', $db_odd->name);

                                        $firstGoalEvent = null;

                                        foreach($gameResult['events'] as $event){
                                            if(strpos($event['text'],'1st Goal')){
                                                $firstGoalEvent = $event['text'];
                                                break;
                                            }
                                        }

                                        $firstGoalTime = null;

                                        if($firstGoalEvent){
                                            $firstGoalTime = preg_replace('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', '', $firstGoalEvent);
                                        }

                                        if(strpos($db_odd->name,'No')){
                                            
                                            if($firstGoalTime && $firstGoalTime < $betGoalTime){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }

                                        } else {

                                            if($firstGoalTime && $firstGoalTime > $betGoalTime){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            } elseif(!$firstGoalTime) {
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                        } 
                                    break;
                                    case 25://late_goal
                                        
                                        $betGoalTime = preg_replace('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', '', $db_odd->name);

                                        $firstGoalEvent = null;

                                        foreach($gameResult['events'] as $event){
                                            if(strpos($event['text'],'Goal')){
                                                $firstGoalEvent = $event['text'];
                                                break;
                                            }
                                        }

                                        $lastGoalTime = null;

                                        if($firstGoalEvent){
                                            $lastGoalTime = preg_replace('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', '', $firstGoalEvent);
                                        }

                                        if(strpos($db_odd->name,'No')){
                                            
                                            if($lastGoalTime && $lastGoalTime > $betGoalTime){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }

                                        } else {

                                            if($lastGoalTime && $lastGoalTime < $betGoalTime){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            } elseif(!$lastGoalTime) {
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                        } 
                                    break;
                                    case 26://time_of_first_goal_brackets
                                        
                                        $betGoalTime = preg_replace('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', '', $db_odd->name);

                                        $firstGoalEvent = null;

                                        foreach($gameResult['events'] as $event){
                                            if(strpos($event['text'],'1st Goal')){
                                                $firstGoalEvent = $event['text'];
                                                break;
                                            }
                                        }

                                        $firstGameGoal = null;

                                        if($firstGoalEvent){
                                            $values = explode('/',$db_odd->name);
                                            $first_limit = $values[0];
                                            $second_limit = $values[1];
                                            $firstGameGoal = preg_replace('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', '', $firstGoalEvent);
                                        }

                                            
                                        if(!$firstGameGoal || $firstGameGoal && ($firstGameGoal < $first_limit || $firstGameGoal > $second_limit)){
                                            $betStatus = false;
                                            $loosingBet = Bet::where('id',$bet->id)->first();
                                            $loosingBet->status = 0;
                                            $loosingBet->save();
                                            break 2;
                                        }
                                    break;
                                    default:
                                       echo "i is not equal to 0, 1 or 2";
                                }

                            } else {
                                if($gameResult['time_status'] == 3){
                                    
                                    switch ($db_odd->name) {
                                        case 'Draw':
                                            if($homeGoalsNbr != $awayGoalsNbr){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                            break;
                                        case $gameResult['home']['name']:
                                            if($homeGoalsNbr < $awayGoalsNbr){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                            break;
                                        case $gameResult['home']['name']:
                                            if($homeGoalsNbr > $awayGoalsNbr){
                                                $betStatus = false;
                                                $loosingBet = Bet::where('id',$bet->id)->first();
                                                $loosingBet->status = 0;
                                                $loosingBet->save();
                                                break 2;
                                            }
                                            break;
                                        default:
                                           echo "i is not equal to 0, 1 or 2";
                                    }
                                }
                            }
                            if($betStatus){
                                $winBet = Bet::where('id',$bet->id)->first();
                                $winBet->status = 1;
                                $winBet->save();
                                //update user solde
                                $userToUpdate = User::where('id',$user->id)->first();
                                $userToUpdate->money = $userToUpdate->money + $winBet->amount_to_win;
                                $userToUpdate->save();
                            }
                        } 
                    }
                }
            }
        }

    }
}

