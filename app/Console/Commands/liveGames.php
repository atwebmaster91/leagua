<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use App\Country;
use App\League;
use App\Equipe;
use App\Prematch;
use App\OddsCategorie;
use App\OddsSubCategorie;
use App\Odd;
use App\Sport;
use Log;

class LiveGames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'live:games';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get the live games';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client();
        
		$this->accessToken =  env('BETSAPI_TOKEN', null);
		$this->api_url = env('BETSAPI_API_URL', null);
		$this->sports = Sport::where('status', 1)->select('id')->pluck('id')->toArray();
		$this->sports = array_values($this->sports);
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        try {
			$results = array();
            
            $live_categorie = OddsCategorie::where('name', 'live')->first();
			//loop all the sports
			foreach ($this->sports as $sportId) {
                
				$url_live = $this->api_url . "/inplay_filter";
				// $url_upcoming_odds = $this->api_url . "/start_sp";
                
				$page = 1;
				$ceuil = 0;
				
				do{
					$data['page'] = $page;
					$data['token'] = $this->accessToken;
					$data['sport_id'] = $sportId;
                    
					$response = $this->client->get($url_live, ['query' => $data]);
					$content = json_decode($response->getBody(true)->getContents(),true);
					$ceuil = ceil( $content['pager']['total'] % $content['pager']['per_page']);
                    
					if(!$content['success']){
						throw new RequestException(
					         "Error",
					         $response
					     );
					}

                    $results =  array_merge($results,$content['results']);
                    
                    $prematch_ids = array();
                    
					foreach ($content['results'] as $result) {

							$countrie = Country::all()->filter(function ($item) use ($result) {
								if(strpos($result['league']['name'],$item->name) !== false){
									return $item;
								}
							})->values()->first();
							$cc = ($countrie) ? $countrie->cc : null;
                            
							$league = League::updateOrCreate([
								'id' => $result['league']['id'],
								'sport_id' => $sportId,
								// 'name' => $result['league']['name']
							],['country_cc' => $cc, 'name' => $result['league']['name']]);
							//$league->country_cc = $cc;
							//$league->save();

							Equipe::firstOrCreate([
								'id' => $result['home']['id'],
								'sport_id' => $sportId,
								'name' => $result['home']['name']
							]);

							Equipe::firstOrCreate([
								'id' => $result['away']['id'],
								'sport_id' => $sportId,
								'name' => $result['away']['name']
							]);

							$prematch = Prematch::firstOrNew([
								'id' => $result['id'],
								'league_id' => $result['league']['id'],
								'home_id' => $result['home']['id'],
								'away_id' => $result['away']['id'],
                            ]);

                            //live game time
							$game_result = $this->api_url . "/result";
							$data['token'] = $this->accessToken;
							$data['event_id'] = $result['id'];
							$response = $this->client->get($game_result, ['query' => $data]);
							$game = json_decode($response->getBody(true)->getContents(),true);
							
							if(array_key_exists('timer', $game['results'][0])){
								if(strlen($game['results'][0]['timer']['ts']) == 1){
									$game['results'][0]['timer']['ts'] = '0'.$game['results'][0]['timer']['ts'];
								}

								$prematch->live_time = $game['results'][0]['timer']['tm'] . ':' . $game['results'][0]['timer']['ts'];	
                            }
                            
                            //get the odds only if the game is still playing
                            if(array_key_exists('time_status', $game['results'][0]) && $game['results'][0]['time_status'] == 1){
                                //update live status
                                $prematch->live = 1;
                                //call the events api https://api.betsapi.com/v1/bet365/event?token=20059-Ke1NJ06zkrnaxM&FI=85193184
                                $game_result = $this->api_url . "/event";
                                $data['token'] = $this->accessToken;
                                $data['FI'] = $result['id'];
                                $response = $this->client->get($game_result, ['query' => $data]);
                                $game_events = json_decode($response->getBody(true)->getContents(),true);
                                
                                //check if game_events is live
                                if(array_key_exists('results', $game_events)){

                                    $prematch->time = $result['time'];
                                    $prematch->time_status = 1;
                                    $prematch->ss = $game_events['results'][0][0]['SS'];
                                    //$prematch->live = 1;
                                    
                                    $prematch->event_id = $result['our_event_id'];
                                    $prematch->save();
                                    
                                    array_push($prematch_ids,$prematch->id);
                                    //create odds_ids array to collect all odds from api and disable the other ones in DB
                                    $odds_ids = array();
                                    //live game Odds
                                    foreach($game_events['results'][0] as $event){
                                        // $odd_sub_categorie = null;
                                        //get the sub category id
                                        if(array_key_exists('type', $event) && $event['type'] == 'MA'){
                                            $sub_cat_name = $event['NA'];
                                            //
                                            if($sub_cat_name == 'Fulltime Result'){
                                                $sub_cat_name = "full_time_result";
                                            } else {
                                                $sub_cat_name = str_replace(" ", "_", $sub_cat_name);
                                                $sub_cat_name = str_replace("-", "_", $sub_cat_name);
                                                $sub_cat_name = strtolower($sub_cat_name);
                                            }
                                            //get or create the sub category
                                            $odd_sub_categorie = OddsSubCategorie::where([
                                                ['name', utf8_encode($sub_cat_name)],
                                                ['description', utf8_encode($sub_cat_name)]
                                            ])->first();

                                            if($odd_sub_categorie === null){
                                                $odd_sub_categorie = new OddsSubCategorie();
                                                $odd_sub_categorie->name = utf8_encode($sub_cat_name);
                                                $odd_sub_categorie->description = utf8_encode($sub_cat_name);
                                                $odd_sub_categorie->categorie_id = $live_categorie->id;
                                                $odd_sub_categorie->save();
                                            };
                                            
                                        }
                                        
                                        if(array_key_exists('OD', $event) && strlen($event['OD']) > 0 && array_key_exists('ID', $event)){
                                            //calculate odds value;
                                            $oddValues = explode('/',$event['OD']);
                                            $nbr1 = $oddValues[0];
                                            $nbr2 = $oddValues[1];
                                            if(intval($nbr2) != 0){
                                                $finalOddValue = (intval($nbr1) / intval($nbr2) + 1);
                                            } else {
                                                $finalOddValue = intval($nbr1);
                                            }

                                            //set the odd values
                                            $oddName = null;
                                            $goals = null;
                                            $header = null;
                                            if(array_key_exists('NA', $event)){
                                                $oddName = $event['NA'];
                                            } elseif(array_key_exists('BS', $event)){
                                                $oddName = $event['BS'];
                                            }
                                            if(array_key_exists('LA', $event) && strlen($event['LA']) > 0){
                                                $goals = $event['LA'];
                                                $header = $event['LA'];
                                            } elseif(array_key_exists('HD', $event) && strlen($event['HD']) > 0){
                                                $goals = $event['HD'];
                                                $header = $event['HD'];
                                            } elseif(array_key_exists('HA', $event) && strlen($event['HA']) > 0){
                                                $goals = $event['HA'];
                                                $header = $event['HA'];
                                            }
   
                                            $finalOddValue = round($finalOddValue, 2);

                                            //check if we need to update existing odd or create a new one
                                            $odd = Odd::firstOrNew([
                                                'odd_api_id' => $event['ID'],
                                                'name' => $oddName,
                                                'goals' => $goals,
                                                'header' => $header,
                                                'prematch_id' => $prematch->id
                                            ]);

                                            $odd->odds = $finalOddValue;
                                            $odd->disabled = $event['SU'];
                                            if($odd_sub_categorie && $odd_sub_categorie->id){
                                                $odd->odds_sub_categorie_id = $odd_sub_categorie->id;
                                            }
                                            $odd->live_odds = 1;
                                            $odd->save();
                                            array_push($odds_ids,$odd->id);
                                            
                                        }
                                    }
                                    
                                    //update old live games odds
                                    $old_prematch_odds = Odd::where([['disabled', 0],['prematch_id',$prematch->id]])->whereNotIn('id', $odds_ids)->update(['disabled' => 1,'live_odds' => 0]);
                                } else {
                                    $prematch->live = 0;
                                    $prematch->time_status = 3;
                                    $prematch->save();
                                }
                            } else if(array_key_exists('time_status', $game['results'][0]) && $game['results'][0]['time_status'] == 3){
                                $prematch = Prematch::where('id',$result['id'])->first();
                                $prematch->live = 0;
                                $prematch->save();
                            }
                            
						}
                    //update old live games
                    $lives = Prematch::where('live',1)->whereHas('league.sport', function($q) use ($sportId){
                        $q->where('sport_id', $sportId );
                    })->whereNotIn('id', $prematch_ids)->update(['live' => 0,'time_status' => 3, 'live_time' => null]);
                    //Log::info($prematch_ids);
				    $page ++;
				} while( $page < $ceuil );	
            }
                        
			return $results;

		} catch (RequestException $e) {
				return $e;
        }
        
        
    }
}

