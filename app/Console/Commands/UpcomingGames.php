<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use App\Country;
use App\League;
use App\Equipe;
use App\Prematch;
use App\OddsCategorie;
use App\OddsSubCategorie;
use App\Odd;
use App\Sport;
use Log;

class UpcomingGames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upcoming:games';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get the upcoming games';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client();
        
		$this->accessToken =  env('BETSAPI_TOKEN', null);
		$this->api_url = env('BETSAPI_API_URL', null);
		$this->sports = Sport::where('status', 1)->select('id')->pluck('id')->toArray();
		$this->sports = array_values($this->sports);
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        try{
			$results = array();
			//unset($this->sports[0]);
			//unset($this->sports[1]);
			foreach ($this->sports as $sportId) {
				$url_upcoming = $this->api_url . "/upcoming";
				$url_upcoming_odds = $this->api_url . "/start_sp";

				$today = date('Y-m-d');
				
				for ($i=0; $i < 3 ; $i++) { 
					$page = 1;
					$ceuil = 0;
					do{
						$data['page'] = $page;
						$data['token'] = $this->accessToken;
						$data['sport_id'] = $sportId;
						$data['day'] = $today;

						$response = $this->client->get($url_upcoming, ['query' => $data]);
						$content = json_decode($response->getBody(true)->getContents(),true);
						$ceuil = ceil( $content['pager']['total'] % $content['pager']['per_page']);
						if(!$content['success']){
							throw new RequestException(
						         "Error",
						         $response
						     );
						}

						$results =  array_merge($results,$content['results']);

						foreach ($content['results'] as $result) {
							/*if($result['time'])*/

							// 30 minutes de crons
							/*$dT = strtotime(date("Y-m-d H:i:s", strtotime("+30 minutes")));
							if($result['time'] >= $dT){*/

								$countrie = Country::all()->filter(function ($item) use ($result) {
									if(strpos($result['league']['name'],$item->name) !== false){
										return $item;
									}
								})->values()->first();
								$cc = ($countrie) ? $countrie->cc : null;

								$league = League::firstOrNew([
									'id' => $result['league']['id'],
									'sport_id' => $sportId,
									//'name' => $result['league']['name']
                                ]);
                                $league->name = $result['league']['name'];
								$league->country_cc = $cc;
								$league->save();

								Equipe::firstOrCreate([
									'id' => $result['home']['id'],
									'sport_id' => $sportId,
									'name' => $result['home']['name']
								]);

								Equipe::firstOrCreate([
									'id' => $result['away']['id'],
									'sport_id' => $sportId,
									'name' => $result['away']['name']
								]);

								$prematch = Prematch::firstOrNew([
									'id' => $result['id'],
									'league_id' => $result['league']['id'],
									'home_id' => $result['home']['id'],
									'away_id' => $result['away']['id']
                                ]);
                                
                                Log::info($prematch);

								$prematch->time = $result['time'];
								$prematch->time_status = $result['time_status'];
								$prematch->ss = $result['ss'];
								$prematch->event_id = $result['our_event_id'];
								$prematch->save();

								//Odds

								$data_odds['FI'] = $prematch->id;
								$data_odds['token'] = $this->accessToken;
								$response_odds = $this->client->get($url_upcoming_odds, ['query' => $data_odds]);
								$content_odds = json_decode($response_odds->getBody(true)->getContents(),true);
								unset($content_odds['results'][0]['FI']);
								unset($content_odds['results'][0]['event_id']);
								unset($content_odds['results'][0]['corners']);
								unset($content_odds['results'][0]['schedule']);
								
								
								foreach ($content_odds['results'][0] as $key => $value) {
									$odd_categorie = OddsCategorie::firstOrNew([
										'name' => $key
									]);
									$odd_categorie->description = $key;
									$odd_categorie->save();

									foreach ($value['sp'] as $key2 => $value2){
										$odd_sub_categorie = OddsSubCategorie::firstOrNew([
											'name' => $key2
										]);
										$odd_sub_categorie->description = $key2;
										$odd_sub_categorie->categorie()->associate($odd_categorie);
										$odd_sub_categorie->save();
										foreach ($value2 as $odd) {
											
											$odd['odds_sub_categorie_id'] = $odd_sub_categorie->id;
											$odd['prematch_id'] = $prematch->id;
											$odd['live_odds'] = 0;
											if(array_key_exists('id',$odd)){
												$odd['odd_api_id'] = $odd['id'];
											}
											
											$oddObj = Odd::firstOrCreate($odd);
										}
										
									};
								};

								// End Odds
								/*}*/
							}

					    $page ++;
					}while( $page < $ceuil );	
				
					$today = date('Ymd', strtotime($today. '+ 1 days'));
				}
			}

			return $results;
		}catch (RequestException $e){
			return $e;
		}
        
    }
}

