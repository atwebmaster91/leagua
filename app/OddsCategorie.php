<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OddsCategorie extends Model
{
    protected $fillable = ['name','description'];
    protected $hidden = ['created_at','updated_at'];
    
    public function subCategorie(){
    	return $this->hasMany(OddsSubCategorie::class,'categorie_id');
    }
}
