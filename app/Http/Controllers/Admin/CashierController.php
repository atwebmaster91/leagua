<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTables;
use App\Traits\CheckDB;
use App\User;
use Auth;

class CashierController extends Controller
{
    use CheckDB;

    public function check_email() {
        try {
            $request = request();
            return response()->json($this->checkDB_email($request));
        } catch (\Exception $e) {
            return response()->json(false);
        }
    }

    public function check_username() {
        try {
            $request = request();
            return response()->json($this->checkDB_username($request));
        } catch (\Exception $e) {
            return response()->json(false);
        }
    }

    public function edit($id) {
        $admins = null;
        if(Auth::user()->hasRole('super-admin')){
            $admins = User::withInactive()->role('admin')->get();
        }
        $user = User::withInactive()->role('cashier')->findOrFail($id);
        return view('back.cashiers.edit',compact('user','admins'));
    }

    public function update() {

        $roles = array(
            'avatar' => 'mimes:jpeg,jpg,png',
            'first_name' => 'required',
            'last_name'  => 'required',
            'user_id'  => ['required', Rule::exists('users','id')],
            'email'  => 'required|email|unique:users,email,'.request('user_id')
        );

        if(request('password')){
            $roles["password"] = "required|between:6,20|confirmed";
            $roles["password_confirmation"] = "between:6,20";
        }

        if(Auth::user()->hasRole('super-admin')){
            $roles["parent_id"] =  ['required', Rule::exists('users','id')];
        }

        request()->validate($roles);

        try {
            
            $admin = User::withInactive()->role('cashier')->findOrFail(request('user_id'));

            if(request('avatar')) {
                $file = request('avatar');
                $filename = time().'.'.$file->getClientOriginalExtension();
                $destinationPath = User::UPLOAD_DIR;
                $thumb_img = Image::make($file->getRealPath())->resize(70, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($destinationPath.'thumbs/'.$filename);
                $file->move($destinationPath, $filename);
                File::delete(public_path(User::UPLOAD_DIR. 'thumbs/'.$admin->avatar));
                File::delete(public_path(User::UPLOAD_DIR. ''.$admin->avatar));
                $admin->avatar = $filename;
            }

            $admin->first_name = request('first_name');
            $admin->last_name = request('last_name');
            $admin->email = request('email');


            if(Auth::user()->hasRole('super-admin')){
                //super admin side
                if(User::find(request('parent_id'))->hasRole('admin')){
                    $admin->parent_id = request('parent_id');
                }else{
                    abort(500,"User parent is not an admin");
                }
            }else{
                //admin side (is connected)
                $admin->parent_id = Auth::user()->id;
            }  

            if(request('password')) 
                $admin->password = Hash::make(request('password'));
            $admin->save();

            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>true,
                    'data'=>$admin,
                    'message'=>'Cashier Updated',
                    'error'=> null
                ],200, [], JSON_NUMERIC_CHECK);
            }

            return redirect()->back()
                ->with('success','Le cassiére a été modifié avec succès'); 

        } catch (\Exception $e) {

            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>false,
                    'data'=>null,
                    'message'=>$e->getMessage(),
                    'error'=> null
                ],412);
            }

            return redirect()->back()
                ->with('error','Un erreur s\'est produite, Le cassiére n\'était pas modifiée. Merci de réessayer.')->withInput(); 
        } 
    }

    public function create() {

        $admins = null;
        if(Auth::user()->hasRole('super-admin')){
            $admins = User::withInactive()->role('admin')->get();
        }
        
        return view('back.cashiers.create',compact('admins'));
    }

    public function store() {

        $roles = array(
            'avatar' => 'mimes:jpeg,jpg,png',
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'  => 'required|email|unique:users,email',
            'username'  => 'required|unique:users,username',
            'password'  => 'required|between:6,20|confirmed',
            'password_confirmation'  => 'required|between:6,20'
        );

        if(Auth::user()->hasRole('super-admin')){
            $roles["parent_id"] =  ['required', Rule::exists('users','id')];
        }

        request()->validate($roles);

        try {
            
            $admin = new User();

            if(request('avatar')) {
                $file = request('avatar');
                $filename = time().'.'.$file->getClientOriginalExtension();
                $destinationPath = User::UPLOAD_DIR;
                $thumb_img = Image::make($file->getRealPath())->resize(70, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($destinationPath.'thumbs/'.$filename);
                $file->move($destinationPath, $filename);
                File::delete(public_path(User::UPLOAD_DIR. 'thumbs/'.$admin->avatar));
                File::delete(public_path(User::UPLOAD_DIR. ''.$admin->avatar));
                $admin->avatar = $filename;
            }

            $admin->first_name = request('first_name');
            $admin->last_name = request('last_name');
            $admin->email = request('email');
            $admin->username = request('username');
            $admin->password = Hash::make(request('password'));

            if(Auth::user()->hasRole('super-admin')){
                //super admin side
                if(User::find(request('parent_id'))->hasRole('admin')){
                    $admin->parent_id = request('parent_id');
                }else{
                    abort(500,"User parent is not an admin");
                }
            }else{
                //admin side (is connected)
                $admin->parent_id = Auth::user()->id;
            }   

            $admin->save();
            $admin->assignRole('cashier');

            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>true,
                    'data'=>$admin,
                    'message'=>'Cashier Created',
                    'error'=> null
                ],200, [], JSON_NUMERIC_CHECK);
            }

            return redirect()->back()
                ->with('success','Le cassiére a été ajouté avec succès'); 

        } catch (\Exception $e) {
            
            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>false,
                    'data'=>null,
                    'message'=>$e->getMessage(),
                    'error'=> null
                ],412);
            }

            return redirect()->back()
                ->with('error','Un erreur s\'est produite, Le cassiére  n\'était pas ajouté. Merci de réessayer.')->withInput(); 
        } 

    }

    public function datatable()
    {
        if(Auth::user()->hasRole('super-admin')){
            $admins = User::withInactive()->role('cashier')->get();
        }else{
            $admins = User::withInactive()->where('parent_id', Auth::user()->id)->role('cashier')->get();
        }
        $admins->makeVisible('money');
        
        return DataTables::of($admins)
            ->editColumn('avatar', function($admin) {
                return '<img src="'.$admin->presentAvatar(true).'" class="img-circle" style="width:40px; height:40px"/>';
            })
            ->editColumn('status', function($admin) {
                $status = ($admin->status == 1) ? 'checked' : '';
                return '<input type="checkbox" '.$status.' data-height="23" data-id="'.$admin->id.'" class="make-switch" data-on="success"  data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default">';
            })
            ->addColumn('parent', function($admin) {
                return $admin->parent()->first()->name();
            })
            ->addColumn('actions', function($admin) {
                return '<center><a href="'.route('cashiers.edit',['id'=>$admin->id]).'">
                            <button type="button" class="tooltips btn btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="top" data-original-title="Modifier caissiére">
                               <i class="fa fa-edit"></i>
                            </button>
                        </a>
                        <a href="javascript:;" onClick="remove(\'cashiers\','.$admin->id.')">
                            <button type="button" class="tooltips btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" data-original-title="Suprrimer caissiére">
                              <i class="fa fa-times"></i>
                            </button>
                        </a>
                        </center>';
            })
            ->rawColumns(['avatar', 'status','actions'])
            ->make(true);
    }

    public function list()
    {
        try {
            if(Auth::user()->hasRole('super-admin')){
                $admins = User::withInactive()->role('cashier')->get();
            }else{
                $admins = User::withInactive()->where('parent_id', Auth::user()->id)->role('cashier')->get();
            }
            $admins->makeVisible('money');  

            return response()->json([
                'status'=>true,
                'data'=>$admins,
                'message'=>'Admin list',
                'error'=> null
            ],200, [], JSON_NUMERIC_CHECK);  

        } catch (\Exception $e) {
            return response()->json([
                'status'=>false,
                'data'=>null,
                'message'=>$e->getMessage(),
                'error'=> null
            ],412);
        }
    }

    public function status() {
        request()->validate(array(
            'id'  => ['required', Rule::exists('users','id')],
        ));
        
        try {
            $admin = User::withInactive()->role('cashier')->find(request('id'));
            if(!$admin){
                if (request()->ajax() || request()->wantsJson()) {
                    return response()->json([
                        'status' => false,
                        'data'=> null,
                        'message' => "Cashier not found",
                        'errors' => null
                    ],404, [], JSON_NUMERIC_CHECK);
                }

                return response()->json(false);
            }
            $admin->status = ($admin->status ==  0) ? 1 : 0;
            $admin->save();

            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>true,
                    'data'=>null,
                    'message'=>'Status Updated',
                    'error'=> null
                ],200, [], JSON_NUMERIC_CHECK);
            }

            return response()->json(true);
        } catch (\Exception $e) {
            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>false,
                    'data'=>null,
                    'message'=>$e->getMessage(),
                    'error'=> null
                ],412);
            }
            return response()->json(false);
        }
    }

    public function destroy() {
        request()->validate([
            'id'  => ['required', Rule::exists('users','id')],
        ]);

        try {
            $admin = User::withInactive()->role('cashier')->find(request('id'));
            if(!$admin){
                if (request()->ajax() || request()->wantsJson()) {
                    return response()->json([
                        'status' => false,
                        'data'=> null,
                        'message' => "User not found",
                        'errors' => null
                    ],404, [], JSON_NUMERIC_CHECK);
                }

                return response()->json(false);
            }
            $admin->delete();

            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>true,
                    'data'=>null,
                    'message'=>'User deleted',
                    'error'=> null
                ],200, [], JSON_NUMERIC_CHECK);
            }

            return response()->json(true);
        } catch (\Exception $e) {
            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>false,
                    'data'=>null,
                    'message'=>$e->getMessage(),
                    'error'=> null
                ],412);
            }
            return response()->json(false);
        }
    }

}
