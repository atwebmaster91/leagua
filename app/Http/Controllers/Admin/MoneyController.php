<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTables;
use Auth;
use App\User;
use App\Money;
use DB;

class MoneyController extends Controller
{
    public function index(){
        $admins = null;
        $selectTitle = "";
        if(Auth::user()->hasRole('super-admin')){
            $admins = User::withInactive()->role('admin')->get();
            $selectTitle = "Admins";
        }else if(Auth::user()->hasRole('admin')){
        	$admins = User::withInactive()->where('parent_id', Auth::user()->id)->role('cashier')->get();
        	$selectTitle = "Caissiéres";
        }else{
        	$admins = User::withInactive()->where('parent_id', Auth::user()->id)->role('client')->get();
        	$selectTitle = "Clients";
        }
    	return view('back.money', compact('admins','selectTitle'));
    }

    public function store() {

        request()->validate(array(
            'type' => 'required|integer|between:0,1',
            'receiver_id'  => ['required', Rule::exists('users','id')],
            'amount'  => 'required|numeric|between:0.001,99999999.999'
        ));

        DB::beginTransaction();
        try {

        	//check child user
            $not_super_admin = true;
	        if(Auth::user()->hasRole('super-admin')){
	        	$not_super_admin = false;
	        	$title = "d'admin";
	            $reciver = User::withInactive()->role('admin')->findOrFail(request('receiver_id'));
	        }else if(Auth::user()->hasRole('admin')){
	        	$title = "de caissiére";
	        	$reciver = User::withInactive()->role('cashier')->findOrFail(request('receiver_id'));
	        }else{
	        	$title = "de client";
	        	$reciver = User::withInactive()->role('client')->findOrFail(request('receiver_id'));
	        }
            //check solde
            if(request('type') == 1){
            	//super admin can make negatif operation
            	if($not_super_admin){
            		if( Auth::user()->money < request('amount')){
            			return redirect()->back()
                		->with('error','Votre solde est insuffisant (max: '. Auth::user()->money.')')->withInput(); 
            		}
            	}
            }else{
            	if($reciver->money < request('amount') ){
            		return redirect()->back()
                		->with('error','Le solde '.$title.' est insuffisant (max: '.$reciver->money.')')->withInput(); 
            	}
            }
			
            $money = new Money();
            $money->type = request('type');
            $money->receiver_id = request('receiver_id');
            $money->amount = request('amount');

			if(Auth::user()->moneySended()->save($money)){
				//mis a jous des soldes
				if(request('type') == 1){
					$reciver->increment('money', request('amount'));
					Auth::user()->decrement('money', request('amount'));
				}else{
					$reciver->decrement('money', request('amount'));
					Auth::user()->increment('money', request('amount'));
				}				
			}

			DB::commit();

            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>true,
                    'data'=>null,
                    'message'=>'Money Sended',
                    'error'=> null
                ],200, [], JSON_NUMERIC_CHECK);
            }

            return redirect()->back()
                ->with('success','L\'opération a été effectuée avec succès'); 

        } catch (\Exception $e) {
        	DB::rollback();

            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>false,
                    'data'=>null,
                    'message'=>$e->getMessage(),
                    'error'=> null
                ],412);
            }

            return redirect()->back()
                ->with('error','Un erreur s\'est produite, L\'opération n\'était pas effectuée. Merci de réessayer.')->withInput(); 
        } 
    }

    public function transactions() {

        if(Auth::user()->hasRole('super-admin')){
            $usersIds = User::withInactive()->get()->pluck('id');
        }else if(Auth::user()->hasRole('admin')){
        	$cashiers = User::withInactive()->where('parent_id', Auth::user()->id)->role('cashier')->pluck('id');
            $clients_cashiers = User::withInactive()->whereIn('parent_id',$parents)->role('client')->get();
           	$usersIds = array_merge($cashiers,$clients_cashiers);
        	$usersIds[] = Auth::user()->id;
        }else{
        	$usersIds = User::withInactive()->where('parent_id', Auth::user()->id)->role('client')->pluck('id');
        	$usersIds[] = Auth::user()->id;
        }


        $transactions = Money::whereIn('sender_id', $usersIds)->orWhereIn('receiver_id', $usersIds)->get();
        return DataTables::of($transactions);


    }

    public function transactionsDatatable() {
        if(Auth::user()->hasRole('super-admin')){
            $usersIds = User::withInactive()->get()->pluck('id');
        }else if(Auth::user()->hasRole('admin')){
        	$cashiersIds = User::withInactive()->where('parent_id', Auth::user()->id)->role('cashier')->pluck('id')->toArray();
            $clients_cashiersIds = User::withInactive()->whereIn('parent_id',$cashiersIds)->role('client')->pluck('id')->toArray();
           	$usersIds = array_merge($cashiersIds,$clients_cashiersIds);
        	$usersIds[] = Auth::user()->id;
        }else{
        	$usersIds = User::withInactive()->where('parent_id', Auth::user()->id)->role('client')->pluck('id');
        	$usersIds[] = Auth::user()->id;
        }


        $transactions = Money::whereIn('sender_id', $usersIds)->orWhereIn('receiver_id', $usersIds)->with(['receiver','sender'])->get();
        
        return DataTables::of($transactions)
        	->editColumn('sender_id', function($transaction) {
                return ($transaction->sender->id == Auth::user()->id) ? '<label class="label label-primary">Me</label>' : $transaction->sender()->first()->name();
            })
            ->editColumn('receiver_id', function($transaction) {
            	return ($transaction->receiver->id == Auth::user()->id) ? '<label class="label label-primary">Me</label>' : $transaction->receiver()->first()->name();
            })
            ->editColumn('amount', function($transaction) {
                return ($transaction->type == 0) ? '-'.$transaction->amount : '+'.$transaction->amount;
            })
            ->editColumn('type', function($transaction) {
                return ($transaction->type == 0) ? '<label class="label label-danger">Retrait</label>' : '<label class="label label-success">Versement</label>';
            })
            ->rawColumns(['sender_id','type','receiver_id'])
            ->make(true);


    }

    public function list()
    {
        try {

            if(Auth::user()->hasRole('super-admin')){
                $usersIds = User::withInactive()->get()->pluck('id');
            }else if(Auth::user()->hasRole('admin')){
                $cashiersIds = User::withInactive()->where('parent_id', Auth::user()->id)->role('cashier')->pluck('id')->toArray();
                $clients_cashiersIds = User::withInactive()->whereIn('parent_id',$cashiersIds)->role('client')->pluck('id')->toArray();
                $usersIds = array_merge($cashiersIds,$clients_cashiersIds);
                $usersIds[] = Auth::user()->id;
            }else{
                $usersIds = User::withInactive()->where('parent_id', Auth::user()->id)->role('client')->pluck('id');
                $usersIds[] = Auth::user()->id;
            }


            $transactions = Money::whereIn('sender_id', $usersIds)->orWhereIn('receiver_id', $usersIds)->with(['receiver','sender'])->get();

            return response()->json([
                'status'=>true,
                'data'=>$transactions,
                'message'=>'Transactions list',
                'error'=> null
            ],200, [], JSON_NUMERIC_CHECK);  

        } catch (\Exception $e) {
            return response()->json([
                'status'=>false,
                'data'=>null,
                'message'=>$e->getMessage(),
                'error'=> null
            ],412);
        }
    }
}
