<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTables;
use App\Traits\CheckDB;
use App\User;
use App\Sport;
use Auth;

class AdminController extends Controller
{
    use CheckDB;

    public function account() {
        try {
            $admin = Auth::user();
            return view("back.admins.account", compact('admin'));
        } catch (\Exception $e) {
            abort(500, 'Server error.');
        }
    }

    public function check_email() {
        try {
            $request = request();
            return response()->json($this->checkDB_email($request));
        } catch (\Exception $e) {
            return response()->json(false);
        }
    }



    public function check_username() {
        try {
            $request = request();
            return response()->json($this->checkDB_username($request));
        } catch (\Exception $e) {
            return response()->json(false);
        }
    }

    public function accountUpdate() {

        $roles = array(
            'avatar' => 'mimes:jpeg,jpg,png',
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'  => 'required|email|unique:users,email,'.Auth::user()->id,
            'username'  => 'required|unique:users,username,'.Auth::user()->id
        );

        if(request('password')){
            $roles["password"] = "required|between:6,20|confirmed";
            $roles["password_confirmation"] = "between:6,20";
        }

        request()->validate($roles);

        try {
            
            $admin = Auth::user();

            if(request('avatar')) {
                $file = request('avatar');
                $filename = time().'.'.$file->getClientOriginalExtension();
                $destinationPath = User::UPLOAD_DIR;
                $thumb_img = Image::make($file->getRealPath())->resize(70, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($destinationPath.'thumbs/'.$filename);
                $file->move($destinationPath, $filename);
                File::delete(public_path(User::UPLOAD_DIR. 'thumbs/'.$admin->avatar));
                File::delete(public_path(User::UPLOAD_DIR. ''.$admin->avatar));
                $admin->avatar = $filename;
            }

            $admin->first_name = request('first_name');
            $admin->last_name = request('last_name');
            $admin->email = request('email');
            $admin->username = request('username');
            if(request('password')) 
            	$admin->password = Hash::make(request('password'));
            $admin->save();

            return redirect()->back()
                ->with('success','Votre compte a été modifié avec succès'); 

        } catch (\Exception $e) {
            return redirect()->back()
                ->with('error','Un erreur s\'est produite, Votre compte n\'était pas modifiée. Merci de réessayer.')->withInput(); 
        } 
    }

    public function edit($id) {
        $user = User::withInactive()->role('admin')->findOrFail($id);
        return view('back.admins.edit',compact('user'));
    }

    public function update() {

        $roles = array(
            'avatar' => 'mimes:jpeg,jpg,png',
            'first_name' => 'required',
            'last_name'  => 'required',
            'user_id'  => ['required', Rule::exists('users','id')],
            'email'  => 'required|email|unique:users,email,'.request('user_id')
        );

        if(request('password')){
            $roles["password"] = "required|between:6,20|confirmed";
            $roles["password_confirmation"] = "between:6,20";
        }

        request()->validate($roles);

        try {
            
            $admin = User::withInactive()->role('admin')->findOrFail(request('user_id'));

            if(request('avatar')) {
                $file = request('avatar');
                $filename = time().'.'.$file->getClientOriginalExtension();
                $destinationPath = User::UPLOAD_DIR;
                $thumb_img = Image::make($file->getRealPath())->resize(70, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($destinationPath.'thumbs/'.$filename);
                $file->move($destinationPath, $filename);
                File::delete(public_path(User::UPLOAD_DIR. 'thumbs/'.$admin->avatar));
                File::delete(public_path(User::UPLOAD_DIR. ''.$admin->avatar));
                $admin->avatar = $filename;
            }

            $admin->first_name = request('first_name');
            $admin->last_name = request('last_name');
            $admin->email = request('email');

            if(request('password')) 
                $admin->password = Hash::make(request('password'));
            $admin->save();

            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>true,
                    'data'=>$admin,
                    'message'=>'Admin Updated',
                    'error'=> null
                ],200, [], JSON_NUMERIC_CHECK);
            }

            return redirect()->back()
                ->with('success','L\'admin a été modifié avec succès'); 

        } catch (\Exception $e) {

            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>false,
                    'data'=>null,
                    'message'=>$e->getMessage(),
                    'error'=> null
                ],412);
            }
            return redirect()->back()
                ->with('error','Un erreur s\'est produite, L\'admin n\'était pas modifiée. Merci de réessayer.')->withInput(); 
        } 
    }


    public function store() {

        request()->validate(array(
            'avatar' => 'mimes:jpeg,jpg,png',
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'  => 'required|email|unique:users,email',
            'username'  => 'required|unique:users,username',
            'password'  => 'required|between:6,20|confirmed',
            'password_confirmation'  => 'required|between:6,20'
        ));

        try {
            
            $admin = new User();

            if(request('avatar')) {
                $file = request('avatar');
                $filename = time().'.'.$file->getClientOriginalExtension();
                $destinationPath = User::UPLOAD_DIR;
                $thumb_img = Image::make($file->getRealPath())->resize(70, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($destinationPath.'thumbs/'.$filename);
                $file->move($destinationPath, $filename);
                File::delete(public_path(User::UPLOAD_DIR. 'thumbs/'.$admin->avatar));
                File::delete(public_path(User::UPLOAD_DIR. ''.$admin->avatar));
                $admin->avatar = $filename;
            }

            $admin->first_name = request('first_name');
            $admin->last_name = request('last_name');
            $admin->email = request('email');
            $admin->username = request('username');
            $admin->password = Hash::make(request('password'));
            $admin->save();

            $admin->assignRole('admin');

            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>true,
                    'data'=>$admin,
                    'message'=>'Admin Created',
                    'error'=> null
                ],200, [], JSON_NUMERIC_CHECK);
            }

            return redirect()->back()
                ->with('success','L\'admin a été ajouté avec succès'); 

        } catch (\Exception $e) {

            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>false,
                    'data'=>null,
                    'message'=>$e->getMessage(),
                    'error'=> null
                ],412);
            }

            return redirect()->back()
                ->with('error','Un erreur s\'est produite, L\'admin  n\'était pas ajouté. Merci de réessayer.')->withInput(); 
        } 

    }

    public function datatable()
    {
        $admins = User::withInactive()->where('id','!=', Auth::user()->id)->role('admin')->get();
        $admins->makeVisible('money');
        
        return DataTables::of($admins)
            ->editColumn('avatar', function($admin) {
                return '<img src="'.$admin->presentAvatar(true).'" class="img-circle" style="width:40px; height:40px"/>';
            })
            ->editColumn('status', function($admin) {
                $status = ($admin->status == 1) ? 'checked' : '';
                return '<input type="checkbox" '.$status.' data-height="23" data-id="'.$admin->id.'" class="make-switch" data-on="success"  data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default">';
            })
            ->addColumn('actions', function($admin) {
                return '<center><a href="'.route('admins.edit',['id'=>$admin->id]).'">
                            <button type="button" class="tooltips btn btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="top" data-original-title="Modifier admin">
                               <i class="fa fa-edit"></i>
                            </button>
                        </a>
                        <a href="javascript:;" onClick="remove(\'admins\','.$admin->id.')">
                            <button type="button" class="tooltips btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" data-original-title="Suprrimer admin">
                              <i class="fa fa-times"></i>
                            </button>
                        </a>
                        </center>';
            })
            ->rawColumns(['avatar', 'status','actions'])
            ->make(true);
    }

    public function list()
    {
        try {
            $admins = User::withInactive()->where('id','!=', Auth::user()->id)->role('admin')->get();
            $admins->makeVisible('money');   

            return response()->json([
                'status'=>true,
                'data'=>$admins,
                'message'=>'Admin list',
                'error'=> null
            ],200, [], JSON_NUMERIC_CHECK);  

        } catch (\Exception $e) {
            return response()->json([
                'status'=>false,
                'data'=>null,
                'message'=>$e->getMessage(),
                'error'=> null
            ],412);
        }
    }

    public function status() {
        request()->validate(array(
            'id'  => ['required', Rule::exists('users','id')],
        ));
        try {
            $admin = User::withInactive()->role('admin')->find(request('id'));
            if(!$admin){
                if (request()->ajax() || request()->wantsJson()) {
                    return response()->json([
                        'status' => false,
                        'data'=> null,
                        'message' => "User not found",
                        'errors' => null
                    ],404, [], JSON_NUMERIC_CHECK);
                }

                return response()->json(false);
            }
            $admin->status = ($admin->status ==  0) ? 1 : 0;
            $admin->save();

            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>true,
                    'data'=>null,
                    'message'=>'Status Updated',
                    'error'=> null
                ],200, [], JSON_NUMERIC_CHECK);
            }

            return response()->json(true);
        } catch (\Exception $e) {
            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>false,
                    'data'=>null,
                    'message'=>$e->getMessage(),
                    'error'=> null
                ],412);
            }
            return response()->json(false);
        }
    }

    public function destroy() {
        request()->validate([
            'id'  => ['required', Rule::exists('users','id')],
        ]);

        try {
            $admin = User::withInactive()->role('admin')->find(request('id'));
            if(!$admin){
                if (request()->ajax() || request()->wantsJson()) {
                    return response()->json([
                        'status' => false,
                        'data'=> null,
                        'message' => "User not found",
                        'errors' => null
                    ],404, [], JSON_NUMERIC_CHECK);
                }

                return response()->json(false);
            }
            $admin->delete();

            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>true,
                    'data'=>null,
                    'message'=>'User deleted',
                    'error'=> null
                ],200, [], JSON_NUMERIC_CHECK);
            }
            return response()->json(true);
        } catch (\Exception $e) {
            if (request()->ajax() || request()->wantsJson()) {
                return response()->json([
                    'status'=>false,
                    'data'=>null,
                    'message'=>$e->getMessage(),
                    'error'=> null
                ],412);
            }
             return response()->json(false);
        }
    }

    public function settings(){

        $sports = Sport::all();
        return view('back.settings.index',compact('sports'));

    }
    

}
