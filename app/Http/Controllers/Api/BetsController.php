<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Mylibs\BetsAPIClient as BetsAPIClient;
use App\Equipe;
use App\Sport;
use App\League;
use App\Prematch;
use App\Country;
use App\Odd;
use App\OddsCategorie;
use App\OddsSubCategorie;
use App\Bet;
use App\User;
use Auth;
use GuzzleHttp\Client;

class BetsController extends Controller
{
    function upcomingEvents(){
      $bet_api = new BetsAPIClient();
      $upc_events = $bet_api->upcomingEvents();
      return response()->json($upc_events);
    }

    function liveEvents(){
      $bet_api = new BetsAPIClient();
      $live_events = $bet_api->liveEvents();
      //return response()->json($live_events->with('sports')->paginate(env('BETSAPI_PAGINATE',5)));
      return response()->json($live_events);
    }

    function deleteEvents(){
      $bet_api = new BetsAPIClient();
      $bet_api->deleteEvents();
      return response()->json(true);
    }
    
    function getEquipes($sport_id = null){

        if($sport_id!= null && !in_array($sport_id, explode(',',env('BETSAPI_SPORT',null)))){
          return response()->json([
            "status" => 0,
            "code" => 403,
            "data" => [],
            "message" => "Sport id inccorect",
          ],403);
        }

      $equipes = ($sport_id != null) 
          ? 
          Equipe::with('sport')->whereHas('sport', function ($query) use ($sport_id) {
          $query->where('id', $sport_id);
        })->paginate(env('BETSAPI_PAGINATE',5)) 
        : 
        Equipe::with('sport')->paginate(env('BETSAPI_PAGINATE',5));

      return response()->json([
        "status" => 1,
        "code" => 200,
        "data" => $equipes,
        "message" => "List equipes",
      ],200);
   }

    function getLeaguesBySport($sport_id = null){

        if($sport_id!= null && !in_array($sport_id, explode(',',env('BETSAPI_SPORT',null)))){
          return response()->json([
            "status" => 0,
            "code" => 403,
            "data" => [],
            "message" => "Sport id inccorect",
          ],403);
        }

      $leagues = ($sport_id != null) 
          ? 
          League::has('prematchs')->with(['sport','prematchs','prematchs.away','prematchs.home','prematchs.odds' => function($q) use($sport_id){

        if($sport_id == 1){ //soocer
            $sub_categ_title = "full_time_result";
        }else if($sport_id == 17){ //Ice Hockey
            $sub_categ_title = "money_line_3_way";
        }else if($sport_id == 16){ //Baseball
            $sub_categ_title = "money_line";
        }else if($sport_id == 13){ //Tennis
            $sub_categ_title = "to_win_match";
        }else if($sport_id == 78){ //Handball
            $sub_categ_title = "money_line_3_way";
        }else{
            $sub_categ_title = null;
        }

        $sub_categ = OddsSubCategorie::where('name',$sub_categ_title)->first();
        if($sub_categ)
          $q->where('odds_sub_categorie_id',$sub_categ->id);
        else
          $q->where('odds_sub_categorie_id',null);
    },'prematchs.countOdds'])->whereHas('sport', function ($query) use ($sport_id) {
          $query->where('id', $sport_id);
        })->withCount('prematchs')->paginate(env('BETSAPI_PAGINATE',5)) 
        : 
        League::has('prematchs')->with('sport')->withCount('prematchs')->paginate(env('BETSAPI_PAGINATE',5));

      return response()->json([
        "status" => 1,
        "code" => 200,
        "data" => $leagues,
        "message" => "List leagues",
      ],200);
   }

    function getLeaguesByCountry($country_cc = null){

        if($country_cc!= null && ! Country::where('cc',$country_cc)->exists()){
          return response()->json([
            "status" => 0,
            "code" => 403,
            "data" => [],
            "message" => "Country cc inccorect",
          ],403);
        }

      $leagues = ($country_cc != null) 
          ? 
          League::has('prematchs')->with('country')->whereHas('country', function ($query) use ($country_cc) {
          $query->where('cc', $country_cc);
        })->withCount('prematchs')->paginate(env('BETSAPI_PAGINATE',5)) 
        : 
        League::has('prematchs')->with('country')->withCount('prematchs')->paginate(env('BETSAPI_PAGINATE',5));

      return response()->json([
        "status" => 1,
        "code" => 200,
        "data" => $leagues,
        "message" => "List leagues",
      ],200);
   }


    function getSports(){
      return response()->json([
        "status" => 1,
        "code" => 200,
        "data" => Sport::paginate(env('BETSAPI_PAGINATE',5)),
        "message" => "List equipes",
      ],200);
    }

    function getUpcomingEvents($sport_id = null){

        if($sport_id!= null && !in_array($sport_id, explode(',',env('BETSAPI_SPORT',null)))){
          return response()->json([
            "status" => 0,
            "code" => 403,
            "data" => [],
            "message" => "Sport id inccorect",
          ],403);
        }
        
      $prematchs = ($sport_id != null) 
    ? 
    Prematch::where('live',0)->with(['league.sport','away','home'])->whereHas('league.sport', function ($query) use ($sport_id) {
      $query->where('id', $sport_id);
    })->paginate(env('BETSAPI_PAGINATE',5)) 
    : 
    Prematch::where('live',0)->with(['league.sport','away','home'])->paginate(env('BETSAPI_PAGINATE',5));

      return response()->json([
        "status" => 1,
        "code" => 200,
        "data" => $prematchs,
        "message" => "List prematch",
      ],200);
    }

    public function getUpcomingEventsByLeague($league_id){
      $league = League::findOrFail($league_id);
      return response()->json([
        "status" => 1,
        "code" => 200,
        "data" => $league->prematchs()->where('live',0)->with(['away','home','league.sport'])->paginate(env('BETSAPI_PAGINATE',5)),
        "message" => "List prematch",
      ],200);   
    }

    public function getLiveEvents(){
      $lives = Prematch::where('live',1)->with(['league.sport','away','home','odds'])->paginate(env('BETSAPI_PAGINATE',5));
      return response()->json([
        "status" => 1,
        "code" => 200,
        "data" => $lives,
        "message" => "List prematch live",
      ],200); 
    }

    public function allLiveEvents(){

      $sub_categ_titles = array("full_time_result","money_line_3_way","money_line","to_win_match","money_line_3_way");
      $sub_categ_ids = OddsSubCategorie::whereIn('name', $sub_categ_titles)->select('id')->pluck('id');
      
      $lives = Prematch::where('live',1)->with(['league.sport','away','home','countLiveOdds','odds' => function($lives) use ($sub_categ_ids) {
            
        $lives->where([['odds.live_odds', 1],['odds.disabled',0]])->whereIn('odds.odds_sub_categorie_id',$sub_categ_ids);

      }])->get();
      // foreach($lives as $live){
      //   $other_odds = OddsCategorie::where('name','live')->with(['subCategorie','subCategorie.odds' => function($other_odds) use ($live){
      //     $other_odds->where([['prematch_id',$live->id],['odds.live_odds', 1],['odds.disabled',0]]);
      //   }])->get();
      //   //$other_odds = Odd::where([['prematch_id',$live->id],['odds.live_odds', 1],['odds.disabled',0]])->whereNotIn('odds_sub_categorie_id',$sub_categ_ids)->get();
      //   $live->otherOdds = $other_odds;
      // }
      return response()->json([
        "status" => 1,
        "code" => 200,
        "data" => $lives,
        "message" => "List prematch live",
      ],200); 
    }

    public function getExternalLiveEvents(){
      $bet_api = new BetsAPIClient();
      $live_events = $bet_api->getLiveEvents();
      //return response()->json($live_events->with('sports')->paginate(env('BETSAPI_PAGINATE',5)));
      return response()->json($live_events); 
    }

    function getCountries($time, $sport_id = null){

      if($time === null || !in_array($time, ['1hour','3hours','today','3days','all'])){
          return response()->json([
            "status" => 0,
            "code" => 403,
            "data" => [],
            "message" => "time is inccorect",
          ],403);
      }
      $active_sports_ids = Sport::where('status', 1)->select('id')->pluck('id');
      $active_sports_ids = $active_sports_ids->toArray();
      $active_sports_ids = array_values($active_sports_ids);

      if($sport_id!= null && !in_array($sport_id, $active_sports_ids)){
          return response()->json([
            "status" => 0,
            "code" => 403,
            "data" => [],
            "message" => "Sport id inccorect",
          ],403);
      }
        
    if ($sport_id != null) {
      $countries = Country::with(['leagues','leagues.prematchs','leagues.prematchs.away','leagues.prematchs.home','leagues.prematchs.odds' =>  function($q) use($sport_id){
        if($sport_id == 1){ //soocer
            $sub_categ_title = "full_time_result";
        }else if($sport_id == 17){ //Ice Hockey
            $sub_categ_title = "money_line_3_way";
        }else if($sport_id == 16){ //Baseball
            $sub_categ_title = "money_line";
        }else if($sport_id == 13){ //Tennis
            $sub_categ_title = "to_win_match";
        }else if($sport_id == 78){ //Handball
            $sub_categ_title = "money_line_3_way";
        }else{
            $sub_categ_title = null;
        }
        $sub_categ = OddsSubCategorie::where('name',$sub_categ_title)->first();
        if($sub_categ)
          $q->where('odds_sub_categorie_id',$sub_categ->id);
        else
          $q->where('odds_sub_categorie_id',null);
      },'leagues.prematchs.countOdds'])->whereHas('leagues.sport', function ($query) use ($sport_id) {
          $query->where('id', $sport_id);
        });
        
    }else{
      $countries = Country::with(['leagues','leagues.prematchs','leagues.prematchs.away','leagues.prematchs.home']);
    } 
      if($time == '1hour'){
        $countries->whereHas('leagues.prematchs', function($q){
          $dT = date("Y-m-d H:i:s", strtotime("+1 hour"));
          $q->where([['time','>=',date("Y-m-d H:i:s")],['time','<=',$dT],['ss', '=', null]]);
        });
      }elseif($time == '3hours'){
        $countries->whereHas('leagues.prematchs', function($q){
          $dT = date("Y-m-d H:i:s", strtotime("+3 hours"));
          $q->where([['time','>=',date("Y-m-d H:i:s")],['time','<=',$dT],['ss', '=', null]]);
        });
      }elseif($time == 'today'){
        $countries->whereHas('leagues.prematchs', function($q){
          $dT = date("Y-m-d 00:00:00", strtotime("+1 day"));
          $q->where([['time', '>=', date('Y-m-d 00:00:00')],['time','<=',$dT],['ss', '=', null]]);
        });
      }elseif($time == '3days'){
        $countries->whereHas('leagues.prematchs', function($q){
          $dT = date("Y-m-d H:i:s", strtotime("+3 days"));
          $q->where([['time', '>=', date("Y-m-d H:i:s")],['time','<=',$dT],['ss', '=', null]]);
        });
      }
      
      if(request('paginate') == 0){
        $countries = $countries->withCount('prematchs')->get();
      } else {
        $countries = $countries->withCount('prematchs')->paginate(env('BETSAPI_PAGINATE',5));
      }
      
      return response()->json([
        "status" => 1,
        "code" => 200,
        "data" => $countries,
        "message" => "List prematch",
      ],200);

    }

  public function getOdds(){

    // $odds = Odd::where('prematch_id',request('prematch_id'))->with(['sub_categorie','sub_categorie.categorie'])->get();
    $prematch_id = request('prematch_id');
    $prematch = Prematch::where('id',$prematch_id)->first();
    
    if($prematch->live == 1){
      $odds = OddsCategorie::where('name', 'live')->with(['subCategorie','subCategorie.odds' => function($odd) use ($prematch){
        $odd->where([['prematch_id', $prematch->id],['disabled',0]]);
      }])->get();
    }
    else {
      $odds = OddsCategorie::with(['subCategorie','subCategorie.odds' => function($odd) use ($prematch){
        $odd->where('prematch_id', $prematch->id);
      }])->get();
    }
    
    return response()->json([
      "status" => 1,
      "code" => 200,
      "data" => $odds,
      "message" => "List odds",
    ],200);
  }

  public function searchUpcomingEvents($term = null){

      if($term === null){
        return response()->json([
          "status" => 0,
          "code" => 404,
          "data" => [],
          "message" => "Term not Found.",
        ],403);
      }

      $term = Str::lower($term);
      $prematchs = Prematch::where('live',0)->whereHas('league.sport', function($q) use ($term){
        $q->where('name', 'LIKE', '%' . $term . '%');
      })->orWhereHas('league.country', function($q) use ($term){
        $q->where('name', 'LIKE', '%' . $term . '%');
      })->orWhereHas('away', function($q) use ($term){
        $q->where('name', 'LIKE', '%' . $term . '%');
      })->orWhereHas('home', function($q) use ($term){
        $q->where('name', 'LIKE', '%' . $term . '%');
      })->orWhereHas('league', function($q) use ($term){
        $q->where('name', 'LIKE', '%' . $term . '%');
      })->with(['league.sport','league.country','away','home'])->get();

      return response()->json([
        "status" => 1,
        "code" => 200,
        "data" => $prematchs,
        "message" => "List prematch",
      ],200);
  }

  public function submitBets(Request $request){

    $teamParlay = $request->teamParlay;

    $result = [];

    try{

      $user = User::where('id', $request->userId)->first();

      $total_amount_paid = 0;

      $client = new Client();

      $game_result = env('BETSAPI_API_URL', null) . "/event";
      $data['token'] = env('BETSAPI_TOKEN',null);
      
      //verify if user has enough money
      if($user->money >= $request->playingAmount){
        
        for($index = 0; $index < count($teamParlay); $index++){
          
          $parlay = $teamParlay[$index];

          for($secondIndex = 0; $secondIndex < count($parlay); $secondIndex++){

            $prematchs = $parlay[$secondIndex]['prematchs'];
            
            for($thirdIndex = 0; $thirdIndex < count($prematchs); $thirdIndex++){

              $prematch = Prematch::where('id', $prematchs[$thirdIndex])->first();
              
              if($prematch && $prematch->ss != null){
                if($prematch->live == 0){
                  return response()->json([
                    "status" => 1,
                    "code" => 404,
                    "data" => $prematch,
                    "message" => "Game has finished",
                  ],404);
                }
              }
            }

            //check odds if they are still availble
            $odds = $parlay[$secondIndex]['odds'];
            
            for($fourthIndex = 0; $fourthIndex < count($odds); $fourthIndex++){
              
              $db_odd = Odd::where('id', $odds[$fourthIndex]['id'])->first();
              
              if($db_odd && $db_odd->disabled == 1){
                  return response()->json([
                    "status" => 1,
                    "code" => 404,
                    "data" => $db_odd,
                    "message" => "Selected odds is no longer availble",
                  ],404);
              }
              //check if odds value changed
              $data['FI'] = $db_odd->prematch_id;
              
              $response = $client->get($game_result, ['query' => $data]);
              $game_events = json_decode($response->getBody(true)->getContents(),true);
              
              if(array_key_exists('results', $game_events)){

                foreach($game_events['results'][0] as $event){
                  
                  if(array_key_exists('ID', $event) && $event['ID'] == $db_odd->odd_api_id){
                    //check if odd still availble
                    if(array_key_exists('ID', $event) && $event['ID'] == 1){
                      return response()->json([
                        "status" => 0,
                        "code" => 403,
                        "data" => $db_odd,
                        "message" => "Selected odds no more availble",
                      ],403);
                    }
                    $odd_values = explode('/',$event['OD'],2);
                    //
                    $new_odd_value = floatval($odd_values[0]) / floatval($odd_values[1]) + 1;
                    
                    if($new_odd_value != $db_odd->odds){
                      //update the odd
                      $db_odd->odds = $new_odd_value;
                      $db_odd->save();
                      //
                      return response()->json([
                        "status" => 0,
                        "code" => 403,
                        "data" => $db_odd,
                        "message" => "Selected odds value changed",
                      ],403);
                    }
                  }
                }
              }
            }
            
            $bet = new Bet();
            // $bet->prematch_id = $parlay[$secondIndex]['prematch_id'];
            // $bet->odd_id = $parlay[$secondIndex]['id'];
            $bet->user_id = $request->userId;
            $bet->played_amount = $parlay[$secondIndex]['userInput'];
            $bet->amount_to_win = $parlay[$secondIndex]['value'] * $parlay[$secondIndex]['userInput'];
            //
            $bet->save();
            //
            $oddIds = array_column($parlay[$secondIndex]['odds'], 'id');
           
            $bet->odds()->sync($oddIds);
            // $oddIdsList = [];
            
            // for($fourthIndex = 0;$fourthIndex < count($parlay[$secondIndex]['odds']); $fourthIndex++){
              
            //   $oddItem = $parlay[$secondIndex]['odds'][$fourthIndex];
              
            //   if(array_key_exists('id',$oddItem)){
            //     array_push($oddIdsList,$oddItem['id']);
            //   } else {
            //     $odd = new Odd();
            //     $odd->name = $oddItem['NA'];
            //     $odd->prematch_id = $prematch->id;
            //     $odd->odds = $oddItem['odds'];
            //     //$odd->odds_sub_categorie_id = 1;
            //     $odd->odd_api_id = $oddItem['FI'];
                
            //     $odd->save();
                
            //     array_push($oddIdsList,$odd->id);
            //   }
            // }
            // $bet->odds()->sync($oddIdsList);
            array_push($result,$bet);
            //caulculate the total amount and compare it to the total amount to pay
            $total_amount_paid = $total_amount_paid + $parlay[$secondIndex]['userInput'];

          }
          //dedicate the playing amount from the user money
          if($total_amount_paid == $request->playingAmount){

            $user->money = $user->money - $total_amount_paid;

            $user->save();

          }

          return response()->json([
            "status" => 1,
            "code" => 200,
            "data" => $result,
            "message" => "Bets Created with success",
          ],200);
        }
          
      } else {

        return response()->json([
          "status" => 1,
          "code" => 404,
          "data" => $user->money,
          "message" => "Solde insuffisant",
        ],404);

      }
    } catch(Exception $e){
      return response()->json([
        "status" => 1,
        "code" => 404,
        "data" => $e,
        "message" => "Error",
      ],404);
    }
  }

  public function userBets($id)
    {
        //
        $bets = Bet::where('user_id',$id)->with(['odds' => function($req){
          $req = $req->with(['prematch' => function($req){
            $req = $req->with(['home','away']);
          }]);
        }])->get();

        return response()->json([
          "status" => 1,
          "code" => 200,
          "data" => $bets,
          "message" => "List bets",
        ],200);
    }

    public function scheduleBetsUpdate(){

      $betsUpdateJob = new GameBetsUpdate();

      $this->dispatch($betsUpdateJob);
  }

  public function addUserFavoris(Request $request){

      $user = User::where('id',$request->user_id)->with('leagues')->first();
      
      if($user){
          $league_id = $request->league_id;
          $ids = array();
          array_push($ids,$league_id);
          $user->leagues()->sync($ids);
      }

      $user = User::where('id',$request->user_id)->with('leagues')->first();

      return $user;
  }

  public function deleteUserFavoris(Request $request){

      $user = User::where('id',$request->user_id)->with('leagues')->first();
      
      if($user){
          $league_id = $request->league_id;
          $ids = array();
          array_push($ids,$league_id);
          $user->leagues()->detach($ids);
      }

      $user = User::where('id',$request->user_id)->with('leagues')->first();

      return $user;
  }

  public function cashout(Request $request){

    $bet = Bet::where('id',$request->betId)->with('odds')->first();
    //
    if($bet && $bet->status == null){

        $client = new Client();

        $game_result = env('BETSAPI_API_URL', null) . "/event";
        $data['token'] = env('BETSAPI_TOKEN',null);
        
        $cash_back_total = 0;

        foreach($bet->odds as $odd){

          $data['FI'] = $odd->prematch_id;

          $response = $client->get($game_result, ['query' => $data]);
          $game_events = json_decode($response->getBody(true)->getContents(),true);
          
          $odd_found = false;

          if(array_key_exists('results', $game_events)){

            foreach($game_events['results'][0] as $event){
              //
              if(array_key_exists('ID', $event) && $event['ID'] == $odd->odd_api_id){
                //
                if($event['NA'] == $odd->name && $event['HA'] == $odd->header && $event['HD'] == $odd->goals){
                  $odd_values = explode('/',$event['OD'],2);
                  //
                  $new_odd_value = (floatval($odd_values[0]) / floatval($odd_values[1])) + 1;
                  //
                  if(count($bet->odds) > 1){
                    $cash_back = $bet->played_amount / $new_odd_value;
                  } else {
                    // $cash = $bet->played_amount / $new_odd_value;
                    // $difference = $bet->played_amount - $cash;
                    // $cash_back = $bet->amount_to_win - $difference;
                    $new_amount_to_win = $bet->played_amount * $new_odd_value;
                    
                    if($new_amount_to_win > $bet->amount_to_win){
                      $difference = $new_amount_to_win - $bet->amount_to_win;
                      $cash_back = $bet->played_amount - $difference;
                    } else {
                      $difference = $bet->amount_to_win - $new_amount_to_win;
                      $cash_back = $bet->played_amount + $difference;
                    }
              
                  }
                  //
                  $cash_back_total += $cash_back;
                  //
                  $odd_found = true;
                  //
                  break 2;
                } else {

                  $bet->amount = 0;
                  $bet->status = 0;

                  $bet->save();
                  return response()->json([
                    "status" => 1,
                    "code" => 404,
                    "data" => $odd,
                    "message" => "Odd lost",
                  ],404);
                }
                
              }

            }

            if(!$odd_found){
              return response()->json([
                "status" => 1,
                "code" => 404,
                "data" => $odd,
                "message" => "Odd not availble anymore",
              ],404);
            }

          }

        }
        //
        $bet->amount = $cash_back_total;
        $bet->status = 1;
        //
        $bet->save();

        //
        $user = User::where('id',$bet->user_id)->first();
        $user->money = $user->money + $bet->amount;
        $user->save();
    } else {
      return response()->json([
        "status" => 1,
        "code" => 404,
        "data" => $bet,
        "message" => "Impossible to Cashout this Bet",
      ],404);
    }

    return $bet;
}

}
