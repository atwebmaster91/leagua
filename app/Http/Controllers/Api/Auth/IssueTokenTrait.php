<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use Route;

trait  IssueTokenTrait{

	public function issueToken(Request $request, $grantType, $scope = ""){


		$params = [
        	'grant_type' => $grantType,
        	'client_id' => $this->client->id,
        	'client_secret' => $this->client->secret,
        	'scope' => $scope,
        ];

        /*if($grantType !== 'social'){*/
            $params['username'] = $request->email;
            $params['password'] = $request->password;
        /*}else{
            $params['id'] = $request->id;
            $params['provider'] = $request->provider;
            $params['provider_user_id'] = $request->provider_user_id;
        }*/

        $request->request->add($params);
        $proxy = Request::create('oauth/token','POST');

        return Route::dispatch($proxy);

	}
}