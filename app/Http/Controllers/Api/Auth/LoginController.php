<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Client;
use App\User;
use Route;
use DB;
use Auth;

class LoginController extends Controller
{

	use IssueTokenTrait;

	private $client;

	public function __construct(){
        $this->client = Client::where([
                'password_client'=>1,
                'revoked'=>0
        ])->first();
	}

    public function login(Request $request){

    	request()->validate([
            'email'  => 'required',
            'password'  => 'required|between:6,20',
        ]);

        $tokens = $this->issueToken($request,'password');
        $content = json_decode($tokens->content());

        if($tokens->status() != 200){
            return response()->json(['status' => false,'data'=> [],'message' => $content->message,'errors' => null ], $tokens->status(), [], JSON_NUMERIC_CHECK);
        }else{
            $user = User::where("email",request('email'))->orWhere('username',request('username'))->first();
            if($user->status == 1){
                $data = array(
                    'user' => $user,
                    'access' => $content
                );
                return response()->json(['status' => true,'data'=> $data,'message' => "User found",'errors' => null],200, [], JSON_NUMERIC_CHECK);
            }else{
                return response()->json(['status' => false,'data'=> [],'message' => "User inactive",'errors' => null], 202, [], JSON_NUMERIC_CHECK);
            }
        }            
    }

    public function refresh(Request $request){
    	request()->validate([
            'refresh_token'  => 'required'
        ]);

        $tokens = $this->issueToken($request,'refresh_token');
        $content = json_decode($tokens->content());

        if($tokens->status() != 200){
            return response()->json(['status' => false,'data'=> [],'message' => $content->message,'errors' => null], $tokens->status(), [], JSON_NUMERIC_CHECK);
        }else{
            return response()->json(['status' => true,'data'=> $content,'message' => "User found",'errors' => null],200, [], JSON_NUMERIC_CHECK);
        }
    }

    public function logout(){
        
        $access_token  = Auth::user()->token();
        DB::table('oauth_refresh_tokens')->where('access_token_id',$access_token->id)
            ->update(['revoked' => true]);
        $access_token->revoke();
        return response()->json(['status' => true,'data'=> [],'message' => "User desconnected",'errors' => null],200, [], JSON_NUMERIC_CHECK);    

		return response()->json([],204);
    }
}
