<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\contact;

class ContactFormController extends Controller
{
    //
    public function submit() {

            $roles = array(
                'firstName' => 'required|string',
                'lastName' => 'required|string',
                'subject' => 'required|string',
                'message' => 'required',
            );

            request()->validate($roles);

            $data = ['email' => request('email'), 'firstName' => request('firstName'), 'lastName' => request('lastName'), 'telephone' => request('telephone'), 'subject' => request('subject'), 'message' => request('message')];
            
            Mail::to('toraa.amin@gmail.com')->send(new contact($data));
            
            // $data = array('name'=> $request->firstName . $request->lastName,'telephone' => $request->telephone, 'message' => $request->message);
    
            // Mail::send(['text'=>'mail'], $data, function($message) {
            //     $message->to('toraa.amin@gmail.com', 'Tutorials Point')->subject
            //         ($request->subject);
            //     $message->from($request->email, $request->firstName . $request->lastName);
            // });

            //return response()->json(null, 200);

    }
}
