<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\AuthenticationException;
use  \Spatie\Permission\Exceptions\UnauthorizedException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->ajax() || $request->wantsJson()) {

            if($exception instanceof ValidationException) {

                return response()->json([
                    'status' => false,
                    'data'=> null,
                    'message' => $exception->getMessage(),
                    'errors' => $exception->validator->errors()
                ], 422);

            }else if($exception instanceof AuthenticationException){
                return response()->json([
                    'status' => false,
                    'data'=> null,
                    'message' => $exception->getMessage(),
                    'errors' => null
                ], 401);

            }else  if ($exception instanceof UnauthorizedException) {
                return response()->json([
                    'status' => false,
                    'data'=> null,
                    'message' => $exception->getMessage(),
                    'errors' => null
                ], 401);
            }else{

                if(method_exists($exception, 'getResponse')){
                    $msg = json_decode((string) $exception->getResponse()->getBody(), true)['message'];
                }elseif(method_exists($exception, 'getMessage')) {
                    $msg = $exception->getMessage();
                    
                }

                if(isset($msg) || empty($msg) ){
                    $msg = "Error";
                }

                if(method_exists($exception, 'getCode')){
                    $code = $exception->getCode();
                }elseif(method_exists($exception, 'getStatusCode')){
                    $code = $exception->getStatusCode();
                }

                if(!isset($code) || $code < 100){
                    $code = 500;
                }
                
                return response()->json([
                    'status' => false,
                    'data'=> null,
                    'message' => $msg,
                    'errors' => null
                ], $code);

            }
        }

        return parent::render($request, $exception);
    }
}
