<?php

namespace App\Traits;

use Illuminate\Http\Request; 
use App\User; 

trait CheckDB
{
    protected function checkDB_email(Request $request)
    {
        try {
            $id = $request->id_admin;
            $count = User::withInactive()->where("id","!=",$id)->where('email', $request->email)->count();
            if($count > 0 ){
                return false;
            }else{
                return true;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    protected function checkDB_username(Request $request)
    {
        try {
            $id = $request->id_admin;
            $count = User::withInactive()->where("id","!=",$id)->where('username', $request->username)->count();
            if($count > 0 ){
                return false;
            }else{
                return true;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

}