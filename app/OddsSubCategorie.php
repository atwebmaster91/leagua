<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OddsSubCategorie extends Model
{
    protected $fillable = ['name','description'];
    protected $hidden = ['created_at','updated_at'];
    
    public function categorie(){
    	 return $this->belongsTo(OddsCategorie::class);
    }

    public function odds(){
    	return $this->hasMany(Odd::class);
    }
}
