<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Malekbenelouafi\LaravelStatus\HasStatus;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable,HasRoles,HasStatus;


    const UPLOAD_DIR = 'back/uploads/admins/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'money'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function childrens(){
        return $this->hasMany(self::class, 'parent_id', 'id' );
    }

    public function parent(){
        return $this->hasOne(self::class, 'id', 'parent_id' );
    }

    public function name(){
        return $this->first_name .' '. $this->last_name;
    }

    public function bets(){
        return $this->hasMany(Bet::class, 'user_id', 'id' );
    }

    public function presentAvatar($thumbs = null) {
        $thumb = ($thumbs === null) ? '' : 'thumbs/';
        if($this->avatar && file_exists(public_path(self::UPLOAD_DIR.''.$thumb.''.$this->avatar))){
            return asset(self::UPLOAD_DIR.''.$thumb.''.$this->avatar);
        }
        return asset('back/uploads/avatar.png');
    }


    public function moneyRecived(){
        return $this->hasMany(Money::class, 'receiver_id', 'id' );
    }

    public function moneySended(){
        return $this->hasOne(Money::class, 'sender_id', 'id' );
    }

    public function findForPassport($identifier) {
            return $this->orWhere('email', $identifier)->orWhere('username', $identifier)->first();
    }

    public function leagues(){
        return $this->belongsToMany('App\League');
    }
}
