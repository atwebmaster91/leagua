<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sport extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['active'];
    public $incrementing = false;
}
