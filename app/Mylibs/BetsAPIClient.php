<?php
namespace App\Mylibs;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use App\Country;
use App\League;
use App\Equipe;
use App\Prematch;
use App\OddsCategorie;
use App\OddsSubCategorie;
use App\Odd;
use App\Sport;

class BetsAPIClient{

	private $api_url = null;
	private $accessToken = null;
	private $client = null;
	private $sports = [];

	public function __construct()
	{
		$this->client = new Client();
		$this->accessToken =  env('BETSAPI_TOKEN', null);
		$this->api_url = env('BETSAPI_API_URL', null);
		$this->sports = Sport::where('status', 1)->select('id')->pluck('id')->toArray();
		$this->sports = array_values($this->sports);
	}


	public function upcomingEvents()
	{
		try{
			$results = array();
			//unset($this->sports[0]);
			//unset($this->sports[1]);
			foreach ($this->sports as $sportId) {
				$url_upcoming = $this->api_url . "/upcoming";
				$url_upcoming_odds = $this->api_url . "/start_sp";

				$today = date('Y-m-d');
				
				for ($i=0; $i < 3 ; $i++) { 
					$page = 1;
					$ceuil = 0;
					do{
						$data['page'] = $page;
						$data['token'] = $this->accessToken;
						$data['sport_id'] = $sportId;
						$data['day'] = $today;

						$response = $this->client->get($url_upcoming, ['query' => $data]);
						$content = json_decode($response->getBody(true)->getContents(),true);
						$ceuil = ceil( $content['pager']['total'] % $content['pager']['per_page']);
						if(!$content['success']){
							throw new RequestException(
						         "Error",
						         $response
						     );
						}

						$results =  array_merge($results,$content['results']);

						foreach ($content['results'] as $result) {
							/*if($result['time'])*/

							// 30 minutes de crons
							/*$dT = strtotime(date("Y-m-d H:i:s", strtotime("+30 minutes")));
							if($result['time'] >= $dT){*/

								$countrie = Country::all()->filter(function ($item) use ($result) {
									if(strpos($result['league']['name'],$item->name) !== false){
										return $item;
									}
								})->values()->first();
								$cc = ($countrie) ? $countrie->cc : null;

								$league = League::firstOrNew([
									'id' => $result['league']['id'],
									'sport_id' => $sportId,
									'name' => $result['league']['name']
								]);
								$league->country_cc = $cc;
								$league->save();

								Equipe::firstOrCreate([
									'id' => $result['home']['id'],
									'sport_id' => $sportId,
									'name' => $result['home']['name']
								]);

								Equipe::firstOrCreate([
									'id' => $result['away']['id'],
									'sport_id' => $sportId,
									'name' => $result['away']['name']
								]);

								$prematch = Prematch::firstOrNew([
									'id' => $result['id'],
									'league_id' => $result['league']['id'],
									'home_id' => $result['home']['id'],
									'away_id' => $result['away']['id']
								]);

								$prematch->time = $result['time'];
								$prematch->time_status = $result['time_status'];
								$prematch->ss = $result['ss'];
								$prematch->event_id = $result['our_event_id'];
								$prematch->save();

								//Odds

								$data_odds['FI'] = $prematch->id;
								$data_odds['token'] = $this->accessToken;
								$response_odds = $this->client->get($url_upcoming_odds, ['query' => $data_odds]);
								$content_odds = json_decode($response_odds->getBody(true)->getContents(),true);
								unset($content_odds['results'][0]['FI']);
								unset($content_odds['results'][0]['event_id']);
								unset($content_odds['results'][0]['corners']);
								unset($content_odds['results'][0]['schedule']);
								
								
								foreach ($content_odds['results'][0] as $key => $value) {
									$odd_categorie = OddsCategorie::firstOrNew([
										'name' => $key
									]);
									$odd_categorie->description = $key;
									$odd_categorie->save();

									foreach ($value['sp'] as $key2 => $value2){
										$odd_sub_categorie = OddsSubCategorie::firstOrNew([
											'name' => $key2
										]);
										$odd_sub_categorie->description = $key2;
										$odd_sub_categorie->categorie()->associate($odd_categorie);
										$odd_sub_categorie->save();
										foreach ($value2 as $odd) {
											
											$odd['odds_sub_categorie_id'] = $odd_sub_categorie->id;
											$odd['prematch_id'] = $prematch->id;
											$odd['live_odds'] = 0;
											if(array_key_exists('id',$odd)){
												$odd['odd_api_id'] = $odd['id'];
											}
											
											$oddObj = Odd::firstOrCreate($odd);
										}
										
									};
								};

								// End Odds
								/*}*/
							}

					    $page ++;
					}while( $page < $ceuil );	
				
					$today = date('Ymd', strtotime($today. '+ 1 days'));
				}
			}

			return $results;
		}catch (RequestException $e){
			$response = $this->StatusCodeHandling($e);
			return $response;
		}
	}

	public function liveEvents()
	{
		try {
			$results = array();

			//https://api.betsapi.com/v1/bet365/inplay?token=YOUR_TOKEN
			$inplay = $this->api_url . "/inplay";
			$inplay_data['token'] = $this->accessToken;
			$response_inplay = $this->client->get($inplay, ['query' => $inplay_data]);
			$inplay_content = json_decode($response_inplay->getBody(true)->getContents(),true);
			
			foreach ($this->sports as $sportId) {
				$url_live = $this->api_url . "/inplay_filter";
				$url_upcoming_odds = $this->api_url . "/start_sp";

				$page = 1;
				$ceuil = 0;
				
				do{
					$data['page'] = $page;
					$data['token'] = $this->accessToken;
					$data['sport_id'] = $sportId;


					$response = $this->client->get($url_live, ['query' => $data]);
					$content = json_decode($response->getBody(true)->getContents(),true);
					$ceuil = ceil( $content['pager']['total'] % $content['pager']['per_page']);

					if(!$content['success']){
						throw new RequestException(
					         "Error",
					         $response
					     );
					}

					$results =  array_merge($results,$content['results']);
					
					foreach ($content['results'] as $result) {

							$countrie = Country::all()->filter(function ($item) use ($result) {
								if(strpos($result['league']['name'],$item->name) !== false){
									return $item;
								}
							})->values()->first();
							$cc = ($countrie) ? $countrie->cc : null;

							$league = League::updateOrCreate([
								'id' => $result['league']['id'],
								'sport_id' => $sportId,
								'name' => $result['league']['name']
							],['country_cc' => $cc]);
							//var_dump($league);die;
							$league->country_cc = $cc;
							$league->save();

							Equipe::firstOrCreate([
								'id' => $result['home']['id'],
								'sport_id' => $sportId,
								'name' => $result['home']['name']
							]);

							Equipe::firstOrCreate([
								'id' => $result['away']['id'],
								'sport_id' => $sportId,
								'name' => $result['away']['name']
							]);

							$prematch = Prematch::firstOrNew([
								'id' => $result['id'],
								'league_id' => $result['league']['id'],
								'home_id' => $result['home']['id'],
								'away_id' => $result['away']['id'],
							]);

							//live game time
							$game_result = $this->api_url . "/result";
							$data['token'] = $this->accessToken;
							$data['event_id'] = $result['id'];
							$response = $this->client->get($game_result, ['query' => $data]);
							$game = json_decode($response->getBody(true)->getContents(),true);
							
							if(array_key_exists('timer', $game['results'][0])){
								if(strlen($game['results'][0]['timer']['ts']) == 1){
									$game['results'][0]['timer']['ts'] = '0'.$game['results'][0]['timer']['ts'];
								}

								$prematch->live_time = $game['results'][0]['timer']['tm'] . ':' . $game['results'][0]['timer']['ts'];	
							}

							if($game['results'][0]['time_status'] == 3){
								$prematch->live = 0;
							}
							
							$prematch->time = $result['time'];
							$prematch->time_status = $result['time_status'];
							$prematch->ss = $result['ss'];
							$prematch->live = 1;
							$prematch->event_id = $result['our_event_id'];
							$prematch->save();

							//Odds
							$c3 = null;
							foreach($inplay_content['results'][0] as $inp){
					
								if($c3 == null && array_key_exists('FI', $inp) && $inp['FI'] == $prematch->id){
									$c3 = $inp['C3'];
									$result['odds'] = array();
								}
			
								if(array_key_exists('OD', $inp) && array_key_exists('FI', $inp) && $c3 == $inp['FI']){
									//check if live odd exist
									$odd = Odd::where('odd_api_id',$inp['ID'])->first();
									//calculate odds value;
									$oddValues = explode('/',$inp['OD']);
									$nbr1 = $oddValues[0];
									$nbr2 = $oddValues[1];
									if(intval($nbr2) != 0){
										$finalOddValue = (intval($nbr1) / intval($nbr2) + 1);
									} else {
										$finalOddValue = intval($nbr1);
									}
									
									$finalOddValue = round($finalOddValue, 2);
									//check if we need to update existing odd or create a new one
									if($odd && $odd->id){
										$odd->odds = $finalOddValue;
										$odd->save();
									}
									else{
										$newOdd = new Odd();
										$newOdd->name = $inp['NA'];
										$newOdd->prematch_id = $prematch->id;
										$newOdd->odds = $finalOddValue;
										$newOdd->live_odds = 1;
										//$odd->odds_sub_categorie_id = 1;
										$newOdd->odd_api_id = $inp['ID'];
										
										$newOdd->save();
									}
									
								}
							}

							// $data_odds['FI'] = $prematch->id;
							// $data_odds['token'] = $this->accessToken;
							// $response_odds = $this->client->get($url_upcoming_odds, ['query' => $data_odds]);
							// $content_odds = json_decode($response_odds->getBody(true)->getContents(),true);
							// unset($content_odds['results'][0]['FI']);
							// unset($content_odds['results'][0]['event_id']);
							// unset($content_odds['results'][0]['corners']);
							// unset($content_odds['results'][0]['schedule']);
							
							
							// foreach ($content_odds['results'][0] as $key => $value) {
							// 	$odd_categorie = OddsCategorie::firstOrNew([
							// 		'name' => $key
							// 	]);
							// 	$odd_categorie->description = $key;
							// 	$odd_categorie->save();

							// 	foreach ($value['sp'] as $key2 => $value2){
							// 		$odd_sub_categorie = OddsSubCategorie::firstOrNew([
							// 			'name' => $key2
							// 		]);
							// 		$odd_sub_categorie->description = $key2;
							// 		$odd_sub_categorie->categorie()->associate($odd_categorie);
							// 		$odd_sub_categorie->save();
							// 		foreach ($value2 as $odd) {
							// 			unset($odd['n2']);
							// 			$odd['odds_sub_categorie_id'] = $odd_sub_categorie->id;
							// 			$odd['prematch_id'] = $prematch->id;
							// 			if(array_key_exists('id',$odd)){
							// 				$odd['odd_api_id'] = $odd['id'];
							// 			}
										
							// 			$oddObj = Odd::firstOrCreate($odd);
							// 		}
									
							// 	};
							// };
						}

				    $page ++;
				}while( $page < $ceuil );	
			}

			return $results;

		} catch (RequestException $e) {
			$response = $this->StatusCodeHandling($e);
			return $response;
		}
	}

	public function getLiveEvents(){

		$results = array();
		//https://api.betsapi.com/v1/bet365/inplay?token=YOUR_TOKEN
		$inplay = $this->api_url . "/inplay";
		$inplay_data['token'] = $this->accessToken;
		$response_inplay = $this->client->get($inplay, ['query' => $inplay_data]);
		$inplay_content = json_decode($response_inplay->getBody(true)->getContents(),true);
		
		//https://api.betsapi.com/v1/bet365/inplay_filter?sport_id=1&token=20059-Ke1NJ06zkrnaxM
		$live_events = $this->api_url . "/inplay_filter";
		$data_odds['token'] = $this->accessToken;
		$response_odds = $this->client->get($live_events, ['query' => $data_odds]);
		$content = json_decode($response_odds->getBody(true)->getContents(),true);
		//https://api.betsapi.com/v1/bet365/result?token=20059-Ke1NJ06zkrnaxM&event_id=83881439
		$events_nbr = 0;
		foreach ($content['results'] as $result) {
			if(in_array($result['sport_id'],$this->sports,false)){
				$game_result = $this->api_url . "/result";
				$data['token'] = $this->accessToken;
				$data['event_id'] = $result['id'];
				$response = $this->client->get($game_result, ['query' => $data]);
				$game = json_decode($response->getBody(true)->getContents(),true);
				
				$result['result'] = $game['results'][0];
				$c3 = null;
				foreach($inplay_content['results'][0] as $inp){
					
					if($c3 == null && array_key_exists('FI', $inp) && $inp['FI'] == $result['id']){
						$c3 = $inp['C3'];
						$result['odds'] = array();
					}

					if(array_key_exists('FI', $inp) && $c3 == $inp['FI']){
						array_push($result['odds'],$inp);
					}
				}
				
				array_push($results,$result);
			}
		}
	
		return $results;
	}
	

	public function deleteEvents()
	{
		$dT = date("Y-m-d H:i:s");
		Prematch::where('time','<',$dT)->delete();	
		return true;
	}

	public function odds_test(){

		$url_upcoming_odds = $this->api_url . "/start_sp";
		$data_odds['FI'] = 79269543;
		$data_odds['token'] = $this->accessToken;
		$response_odds = $this->client->get($url_upcoming_odds, ['query' => $data_odds]);
		$content_odds = json_decode($response_odds->getBody(true)->getContents(),true);
		unset($content_odds['results'][0]['FI']);
		unset($content_odds['results'][0]['event_id']);
		unset($content_odds['results'][0]['corners']);
		foreach ($content_odds['results'][0] as $key => $value) {
			$odd_categorie = OddsCategorie::firstOrNew([
				'name' => $key
			]);
			$odd_categorie->description = $key;
			$odd_categorie->save();

			foreach ($value['sp'] as $key2 => $value2){
				$odd_sub_categorie = OddsSubCategorie::firstOrNew([
					'name' => $key2
				]);
				$odd_sub_categorie->description = $key2;
				$odd_sub_categorie->categorie()->associate($odd_categorie);
				$odd_sub_categorie->save();
				foreach ($value2 as $odd) {
					$odd_sub_categorie->odds()->delete();
					$odd['odds_sub_categorie_id'] = $odd_sub_categorie->id;
					Odd::insert([
						$odd
					]);
				}
				
			};
		};
	}

	public function StatusCodeHandling($e)
	{
		if($e->getResponse()){
			if ($e->getResponse()->getStatusCode() == '422'){
				$response = json_decode($e->getResponse()->getBody(true)->getContents());
				return $response;
			}elseif ($e->getResponse()->getStatusCode() == '500'){
				$response = json_decode($e->getResponse()->getBody(true)->getContents());
				return $response;
			}elseif ($e->getResponse()->getStatusCode() == '401'){
				$response = json_decode($e->getResponse()->getBody(true)->getContents());
				return $response;
			}elseif ($e->getResponse()->getStatusCode() == '403'){
				$response = json_decode($e->getResponse()->getBody(true)->getContents());
				return $response;
			}else{
				$response = json_decode($e->getResponse()->getBody(true)->getContents());
				return $response;
			}
		}
	}
 
}