<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Odd extends Model
{
   protected $fillable = ['live_odds','disabled','odd_api_id','name','handicap','goals','opp','odds','header','odds_sub_categorie_id','prematch_id'];

   protected $hidden = ['created_at','updated_at'];

   public function sub_categorie(){
   	 	return $this->belongsTo(OddsSubCategorie::class,'odds_sub_categorie_id');
   }

   public function bets(){
      return $this->belongsToMany('App\Bet');
   }

   public function prematch(){
      return $this->belongsTo('App\Prematch','prematch_id');
   }
}
