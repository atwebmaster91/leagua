<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $primaryKey = 'cc';
    public $incrementing = false;

    public function leagues(){
    	return $this->hasMany(League::class);
    }

    function prematchs()
    {
        return $this->hasManyThrough('App\Prematch', 'App\League');
    }
}
