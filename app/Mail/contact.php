<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        
        return $this->from($this->data['email'])
                    ->view('mails.demo')
                    ->with([
                        'data' => $this->data,
                      ]);

        // return $this->view('emails/Emails')
        //         ->subject($this->data->subject)
        //         ->attach(storage_path('app/' . $this->theTransaction['invoice']));
    }
}
