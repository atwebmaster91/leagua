<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Money extends Model
{
    protected $table = "moneys";

    public function receiver(){
    	return $this->hasOne(User::class, 'id', 'receiver_id');
    }

    public function sender(){
    	return $this->hasOne(User::class, 'id', 'sender_id');
    }
}
