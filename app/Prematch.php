<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Prematch extends Model
{
	protected $table = 'prematchs';
	protected $fillable = ['id','live_time','league_id','home_id','away_id','time','time_status','ss','event_id'];
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $casts = ['time' => 'date'];


    // public function getTimeAttribute($value){
    // 	return (new Carbon(strtotime($value)))->format('Y-m-d H:i:s');
    // }

    public function league(){
    	return $this->belongsTo(League::class);
    }
    public function home(){
    	return $this->belongsTo(Equipe::class,'home_id');
    }
    public function away(){
    	return $this->belongsTo(Equipe::class,'away_id');
    }

    public function odds(){
        return $this->hasMany(Odd::class);
    }

    public function countOdds(){
        return $this->hasOne(Odd::class)->selectRaw('prematch_id, count(*) as total')
        ->groupBy('prematch_id');
    }

    public function countLiveOdds(){
        return $this->hasOne(Odd::class)->where([['live_odds',1],['disabled',0]])->selectRaw('prematch_id, count(*) as total')
        ->groupBy('prematch_id');
    }
}
