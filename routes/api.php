<?php

Route::group(['prefix' => 'v1'], function(){
	Route::group(['prefix' => 'crons'], function(){
		Route::get('upcomingEvents', 'Api\BetsController@upcomingEvents');
		Route::get('liveEvents', 'Api\BetsController@liveEvents');
		Route::get('deleteEvents', 'Api\BetsController@deleteEvents');
	});

	Route::get('equipes/{sport_id?}', 'Api\BetsController@getEquipes');
	Route::get('sports', 'Api\BetsController@getSports');
	Route::get('leagues/s/{sport_id?}', 'Api\BetsController@getLeaguesBySport');
	Route::get('leagues/c/{sport_id?}', 'Api\BetsController@getLeaguesByCountry');
	Route::get('upcomingEvents/{sport_id?}', 'Api\BetsController@getUpcomingEvents');
	Route::get('upcomingEvents/l/{league_id}', 'Api\BetsController@getUpcomingEventsByLeague');
	Route::get('liveEvents', 'Api\BetsController@getLiveEvents');
	Route::get('externalLiveEvents', 'Api\BetsController@getExternalLiveEvents');
	Route::get('allLiveEvents', 'Api\BetsController@allLiveEvents');
	Route::get('countries/{time}/{sport_id?}', 'Api\BetsController@getCountries');
	Route::get('odds/{prematch_id}', 'Api\BetsController@getOdds');
	Route::get('search/{term?}', 'Api\BetsController@searchUpcomingEvents');
	Route::post('submitBet', 'Api\BetsController@submitBets');
	Route::get('/bets/{user_id}', 'Api\BetsController@userBets');
	Route::post('addFavoris','Api\BetsController@addUserFavoris');
	Route::post('deleteFavoris','Api\BetsController@deleteUserFavoris');
	Route::post('SendMail','ContactFormController@submit');
	Route::post('search','Api\BetsController@search');
	Route::post('create', 'Auth\PasswordResetController@create');
    Route::get('find/{token}', 'Auth\PasswordResetController@find');
	Route::post('reset', 'Auth\PasswordResetController@reset');
	Route::post('cash-out', 'Api\BetsController@cashout');
	

	Route::group(['namespace' => 'Api\Auth'], function()
	{
		Route::post('login', 'LoginController@login');
		Route::post('refresh', 'LoginController@refresh');
		
	});

	Route::group(['middleware' => 'auth:api'], function () 
	{
		Route::post('logout', 'Api\Auth\LoginController@logout');

		Route::group(['namespace'=>'Admin'], function() {

			Route::group(['prefix'=>'admins','middleware' => 'role:super-admin'], function()
			{
				Route::get('/', 'AdminController@list');
				Route::post('store', 'AdminController@store');
				Route::post('update', 'AdminController@update');
	      		Route::post('status', 'AdminController@status');
		        Route::post('delete', 'AdminController@destroy');
			});

		   	Route::group(['prefix'=>'cashiers','middleware' => 'role:super-admin|admin'], function()
		   	{
		   		Route::get('/', 'CashierController@list');
				Route::post('store', 'CashierController@store');
				Route::post('update', 'CashierController@update');
	      		Route::post('status', 'CashierController@status');
		        Route::post('delete', 'CashierController@destroy');
	    	});

		   	Route::group(['prefix'=>'clients'], function() {
	      		Route::get('/', 'ClientController@list');
	      		Route::post('store', 'ClientController@store');
				Route::post('update', 'ClientController@update');
				Route::post('updateClient', 'ClientController@updateClient');
	      		Route::post('status', 'ClientController@status');
	        	Route::post('/delete', 'ClientController@destroy');
	    	});

		   	Route::group(['prefix'=>'money'], function() {
	      		Route::post('/store', 'MoneyController@store');
	    	});

		   	Route::group(['prefix'=>'transactions'], function() {
		   		Route::get('/', 'MoneyController@list');
			});
			
		 });
	});
});