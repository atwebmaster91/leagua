<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth', 'role:client']], function(){
	Route::get('/home', 'HomeController@index')->name('user.home');
});

Route::group(['prefix' => 'backstage'], function(){

	Auth::routes();	

	Route::group(['middleware' => ['auth', 'role:super-admin|admin|cashier']], function(){

		Route::group(['namespace'=>'Admin'], function() {

			Route::get('/', 'DashboardController@index')->name('admin.dashboard');
			Route::get('check_email', 'AdminController@check_email')->name('admin.check_email');
			Route::get('check_username', 'AdminController@check_username')->name('admin.check_username');


			Route::group(['prefix' => 'account'], function(){
				Route::get('/', 'AdminController@account')->name('admin.account');
				Route::post('update', 'AdminController@accountUpdate')->name('admin.account.update');
			});

		   	Route::group(['prefix'=>'admins','middleware' => 'role:super-admin', 'as' =>'admins.'], function() {
	      		Route::view('/', 'back.admins.index')->name('index');
	      		Route::post('datatable', 'AdminController@datatable')->name('datatable');
  		      	Route::view('create', 'back.admins.create')->name('create');
	      		Route::post('store', 'AdminController@store')->name('store');
	      		Route::get('edit/{id}', 'AdminController@edit')->name('edit')->where('id', '[0-9]+');
	      		Route::post('update', 'AdminController@update')->name('update');
	      		Route::post('status', 'AdminController@status')->name('status');
	        	Route::post('/delete', 'AdminController@destroy')->name('destroy');
	    	});

		   	Route::group(['prefix'=>'cashiers','middleware' => 'role:super-admin|admin', 'as' =>'cashiers.'], function() {
	      		Route::view('/', 'back.cashiers.index')->name('index');
	      		Route::post('datatable', 'CashierController@datatable')->name('datatable');
  		      	Route::get('create', 'CashierController@create')->name('create');
	      		Route::post('store', 'CashierController@store')->name('store');
	      		Route::get('edit/{id}', 'CashierController@edit')->name('edit')->where('id', '[0-9]+');
	      		Route::post('update', 'CashierController@update')->name('update');
	      		Route::post('status', 'CashierController@status')->name('status');
	        	Route::post('/delete', 'CashierController@destroy')->name('destroy');
	    	});

		   	Route::group(['prefix'=>'clients', 'as' =>'clients.'], function() {
	      		Route::view('/', 'back.clients.index')->name('index');
	      		Route::post('datatable', 'ClientController@datatable')->name('datatable');
  		      	Route::get('create', 'ClientController@create')->name('create');
	      		Route::post('store', 'ClientController@store')->name('store');
	      		Route::get('edit/{id}', 'ClientController@edit')->name('edit')->where('id', '[0-9]+');
	      		Route::post('update', 'ClientController@update')->name('update');
	      		Route::post('status', 'ClientController@status')->name('status');
	        	Route::post('/delete', 'ClientController@destroy')->name('destroy');
	    	});


		   	Route::group(['prefix'=>'money', 'as' =>'money.'], function() {
	      		Route::get('/', 'MoneyController@index')->name('index');
	      		Route::post('/store', 'MoneyController@store')->name('store');
	    	});

		   	Route::group(['prefix'=>'transactions', 'as' =>'transactions.'], function() {
	      		Route::view('/', 'back.transactions')->name('index');
	      		Route::post('/datatable', 'MoneyController@transactionsDatatable')->name('datatable');
			});
			
			Route::group(['prefix' => 'settings'], function(){
				Route::get('/', 'AdminController@settings')->name('settings.index');
				Route::post('/', 'AdminController@sportsCreate')->name('settings.sports.create');
				//Route::post('update', 'AdminController@accountUpdate')->name('admin.account.update');
			});

	   	});

	});	

	Route::get('/logout', 'Auth\LoginController@logout')->name('backstagelogout');
});

Route::get('/{any}', function () {
    return view('welcome');
})->where('any', '.*');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
