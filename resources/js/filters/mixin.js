export default {
    methods: {
        updateCartTotal(selectedOddsInCart) {
            var vm = this;
            //check how many games are selecteds in the odds
            let teamsNbr = vm.getTeamsNbr(selectedOddsInCart);
            //
            vm.teamParlayResult = []
            //create the teamParlays
            if (teamsNbr > 1) {
                for (let teamNbrIndex = 1; teamNbrIndex <= teamsNbr; teamNbrIndex++) {
                    let parlay = [];
                    let oddsCombined = [];
                    let prematchs = [];
                    let odds = selectedOddsInCart;
                    let devider = teamNbrIndex;
                    //
                    let value = 1;
                    let game = '';
                    //
                    for (let index = 0; index < odds.length; index++) {
                        parlay = vm.calculateIntegrale(teamNbrIndex, index, value, game, oddsCombined, prematchs, parlay, devider);
                        if (index + 1 == odds.length && parlay.length > 0) {
                            vm.teamParlayResult.push(parlay);
                        }

                    }
                }
            } else if (teamsNbr == 1) {
                let parlay = [];
                let oddsCombined = [];
                oddsCombined.push(selectedOddsInCart[0]);
                let prematchs = [];
                prematchs.push(selectedOddsInCart[0].prematch_id);
                vm.calculate(selectedOddsInCart[0].odds, '1', oddsCombined, prematchs, 1, parlay);
                vm.teamParlayResult.push(parlay);
            }
            return vm.teamParlayResult;

        },
        calculateIntegrale(teamNbrIndex, oldIndex, value, game, oddsCombined, prematchs, parlay, devider) {
            var vm = this;
            let odds = vm.selectedOddsInCart;
            //
            let copyValue = value;
            let copyGame = game;
            let copyOddsCombined = oddsCombined;
            let copyPrematchs = prematchs;

            for (let index = oldIndex; index < odds.length; index++) {
                let copyTeamNbrIndex = teamNbrIndex;
                if (odds[index].prematch_id != odds[oldIndex].prematch_id) {
                    //
                    value = copyValue;
                    game = copyGame;
                    oddsCombined = copyOddsCombined;
                    prematchs = copyPrematchs;
                    //
                    value = value == 1 ? (parseFloat(odds[index].odds) * parseFloat(odds[oldIndex].odds)) : (value *
                        parseFloat(odds[index].odds));
                    //
                    game = game.length != 0 ? (game + '/' + (index + 1)) : ((oldIndex + 1) + '/' + (index + 1));
                    //
                    if (oddsCombined.length == 0) {
                        oddsCombined.push(odds[oldIndex]);
                    }
                    oddsCombined.push(odds[index]);
                    //
                    if (prematchs.length == 0) {
                        prematchs.push(odds[oldIndex].prematch_id);
                    }
                    prematchs.push(odds[index].prematch_id);
                    //
                    copyTeamNbrIndex--;
                    //
                    if (copyTeamNbrIndex >= 2) {
                        vm.calculateIntegrale(copyTeamNbrIndex, index, value, game, oddsCombined, prematchs, parlay, devider);
                    }
                    //
                    if (copyTeamNbrIndex == 1) {
                        let oddsArray = [...oddsCombined];
                        let prematchsArray = [...prematchs];
                        vm.calculate(value, game, oddsArray, prematchsArray, devider, parlay);
                        value = 1;
                        game = '';
                        oddsCombined.splice(0, oddsCombined.length);
                        copyTeamNbrIndex = teamNbrIndex;
                    }
                }
            }
            return parlay;
        },
        calculate(value, game, oddsCombined, prematchs, devider, parlay) {
            let exist = parlay.some(obj => obj.x === value);
            if (!exist) {
                let finalValue = (value / devider);
                parlay.push({
                    game: game,
                    value: finalValue.toFixed(2),
                    odds: oddsCombined,
                    prematchs: prematchs,
                    x: value
                });
            }
        },
        getTeamsNbr(selectedOddsInCart) {

            var vm = this;

            let odds = [];

            var index = selectedOddsInCart.length;

            while (index--) {
                let found = odds.filter((odd) => odd.prematch_id === selectedOddsInCart[index].prematch_id)
                    .length;
                if (found == 0) {
                    odds.push(selectedOddsInCart[index]);
                }
            }

            return odds.length;
        }
    }
}