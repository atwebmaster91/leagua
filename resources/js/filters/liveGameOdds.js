export default (value) => {
    var fields = value.split('/');
    var nbr1 = parseInt(fields[0]);
    var nbr2 = parseInt(fields[1]);
    let result = nbr1/nbr2;
    result = result + 1;
    return result.toFixed(3).toString();
} 