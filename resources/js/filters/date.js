export default (value) => {
    const date = new Date(value)
    return date.toLocaleTimeString(['en-US'], {hour: 'numeric', minute: 'numeric', hour12: false}).replace("AM","").replace("PM","")
} 