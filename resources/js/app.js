
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import App from './components/App'
//import('../../public/integration/css/style.css');
//
import VueRouter from 'vue-router'
Vue.use(VueRouter);
import ToggleButton from 'vue-js-toggle-button'
Vue.use(ToggleButton);
import vueResource from 'vue-resource'
Vue.use(vueResource);
import VueSession from 'vue-session'
Vue.use(VueSession)
import VueAgile from 'vue-agile'
Vue.use(VueAgile)
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)
import DateFilter from './filters/date' // Import date
Vue.filter('date', DateFilter ) // register filter globally
import ReplaceUnderscore from './filters/string' // Import date
Vue.filter('ReplaceUnderscore', ReplaceUnderscore ) // register filter globally
import liveGamesOdds from './filters/liveGameOdds' // Import date
Vue.filter('liveGamesOdds', liveGamesOdds ) // register filter globally
import Notifications from 'vue-notification'
Vue.use(Notifications)
import VueMaterial from 'vue-material'
Vue.use(VueMaterial)
import VSwitch from 'v-switch-case'
Vue.use(VSwitch)
import vClickOutside from 'v-click-outside'
Vue.use(vClickOutside)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('header-component', require('./components/HeaderComponent.vue').default);
Vue.component('footer-component', require('./components/FooterComponent.vue').default);
Vue.component('sport-list-component', require('./components/sports/SportListComponent.vue').default);
Vue.component('country-games-list-component', require('./components/sports/CountryGamesListComponent.vue').default);
Vue.component('match-all-odds-component', require('./components/matchs/MatchAllOddsComponent.vue').default);
Vue.component('selected-odds-cart-component', require('./components/matchs/SelectedOddsCartComponent.vue').default);
Vue.component('parlay-options-display-component', require('./components/matchs/ParlayOptionsDisplayComponent.vue').default);
Vue.component('my-bets-component', require('./components/user/MyBetsComponent.vue').default);
Vue.component('main-menu-component', require('./components/MainMenuComponent.vue').default);
Vue.component('scroll-timeline-component', require('./components/ScrollTimelineComponent.vue').default);
Vue.component('live-bets-component', require('./components/LiveBetsComponent.vue').default);
//

const HomePageComponent = () => import('./components/HomePageComponent.vue');
const MyBetsComponent = () => import('./components/user/MyBetsComponent.vue');
const HeaderComponent = () => import('./components/HeaderComponent.vue');
const FooterComponent = () => import('./components/FooterComponent.vue');
const LiveBetsComponent = () => import('./components/LiveBetsComponent.vue');
const ContactPageComponent = () => import('./components/ContactPageComponent.vue');
const MyProfileComponent = () => import('./components/user/MyProfileComponent.vue');
const SearchResultComponent = () => import('./components/matchs/SearchResultComponent.vue');
const ConditionUtilisationComponent = () => import('./components/generic/ConditionUtilisationComponent.vue');
const ReglementComponent = () => import('./components/generic/ReglementComponent.vue');
const ResponsableGameComponent = () => import('./components/generic/ResponsableGameComponent.vue');
const PrivacyComponent = () => import('./components/generic/PrivacyComponent.vue');
const TermsAndConditionsComponent = () => import('./components/generic/TermsAndConditionsComponent.vue');
const AboutUsComponent = () => import('./components/generic/AboutUsComponent.vue');
const ForgotPasswordComponent = () => import('./components/user/ForgotPasswordComponent.vue');
const PasswordResetComponent = () => import('./components/user/PasswordResetComponent.vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home-page-component',
            component: HomePageComponent,
            props: true
        },
        {
            path: '/my-bets',
            name: 'my-bets-component',
            component: MyBetsComponent,
            props: true
        },
        {
            path: '/live-bets',
            name: 'live-bets-component',
            component: LiveBetsComponent,
            props: true
        },
        {
            path: '/contact',
            name: 'contact-page-component',
            component: ContactPageComponent,
            props: true
        },
        {
            path: '/profile',
            name: 'my-profile-component',
            component: MyProfileComponent,
            props: true
        },
        {
            path: '/search-results',
            name: 'search-results-component',
            component: SearchResultComponent,
            props: true
        },
        {
            path: '/condition-utilisation',
            name: 'condition-utilisation-component',
            component: ConditionUtilisationComponent,
            props: true
        },
        {
            path: '/reglements',
            name: 'reglements-component',
            component: ReglementComponent,
            props: true
        },
        {
            path: '/jeu-responsable',
            name: 'responsable-game-component',
            component: ResponsableGameComponent,
            props: true
        },
        {
            path: '/privacy',
            name: 'privacy-component',
            component: PrivacyComponent,
            props: true
        },
        {
            path: '/terms-and-condition',
            name: 'terms-and-condition-component',
            component: TermsAndConditionsComponent,
            props: true
        },
        {
            path: '/qui-some-nous',
            name: 'about-us-component',
            component: AboutUsComponent,
            props: true
        },
        {
            path: '/mot-de-passe-oublier',
            name: 'forgot-password-component',
            component: ForgotPasswordComponent,
            props: true
        },
        {
            path: '/changer-mot-de-passe/:token?',
            name: 'password-reset-component',
            component: PasswordResetComponent,
            props: true
        }
    ],
});

const app = new Vue({
    el: '#app',
    components: {
        App
    },
    router,
});
