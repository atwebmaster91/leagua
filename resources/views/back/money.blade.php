@extends('layouts.back')
@section('title') Versement/Retrait @stop

@section('style')
    <link href="{{ asset('back/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" id="theme" rel="stylesheet">
    <link href="{{ asset('back/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Versement / Retrait</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Tableau de bord</a></li>
                <li class="breadcrumb-item active">Versement / Retrait</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post"  action="{{ route('money.store') }}" id="money">
                            @csrf
                            <div class="form-body">
                                @include('includes.msg')
                                <div class="row">
                                    <div class="col-md-5 m-auto">
                                        <div class="form-group">
                                            <label class="control-label">Type
                                                <span class="text-danger"> * </span>
                                            </label>
                                            <select class="select2 form-control" data-placeholder='' style="width: 100%; height:36px;" name="type">
                                                <option></option>
                                                <option value="0">Versement</option>
                                                <option value="1">Retrait</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">{{ $selectTitle }}
                                                <span class="text-danger"> * </span>
                                            </label>
                                            <select class="select2 form-control" data-placeholder='' style="width: 100%; height:36px;" name="receiver_id">
                                                <option></option>
                                                @foreach($admins as $admin)
                                                    <option value="{{ $admin->id }}" {{ (old("receiver_id") != $admin->id ?: "selected") }}>{{ $admin->name() }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Montant
                                                <span class="text-danger"> * </span>
                                            </label>
                                            <input name="amount" type="text" value="{{ old('amount') }}" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-5 m-auto">
                                    	<div class="row">
                                    		<div class="col-6">
                                    			<a href="{{ route('admin.dashboard') }}" class="btn btn-outline-info">Annuler</a>
                                    		</div>
                                    		<div class="col-6 text-right">
                                    			<button type="submit" class="btn waves-effect waves-light btn-success">Valider</button>
                                    		</div>
                                    	</div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection


@section('script')
    <script src="{{ asset('back/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <script src="{{ asset('back/plugins/jquery-validation/js/jquery.validate.js') }}" type="text/javascript"></script>
    <script src="{{ asset('back/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('back/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('back/js/form-validation.js') }}" type="text/javascript"></script>
@endsection