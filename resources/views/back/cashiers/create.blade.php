@extends('layouts.back')
@section('title') Ajouter cassiére @stop

@section('style')
    <link href="{{ asset('back/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" id="theme" rel="stylesheet">
    <link href="{{ asset('back/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Ajouter cassiére</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Tableau de bord</a></li>
                <li class="breadcrumb-item"><a href="{{ route('cashiers.index') }}">Cassiéres</a></li>
                <li class="breadcrumb-item active">Ajouter cassiére</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post"  action="{{ route('cashiers.store') }}" id="cashier_create" enctype="multipart/form-data">
                            @csrf
                            <div class="form-body">
                                @include('includes.msg')
                                <div class="row">
                                    <div class="col-md-4">
                                        @hasanyrole('super-admin')
                                        <div class="form-group">
                                            <label class="control-label">Admin
                                                <span class="text-danger"> * </span>
                                            </label>
                                            <select class="select2 form-control" data-placeholder='Select Admin' style="width: 100%; height:36px;" name="parent_id">
                                                <option></option>
                                                @foreach($admins as $admin)
                                                    <option value="{{ $admin->id }}" {{ (old("parent_id") != $admin->id ?: "selected") }}>{{ $admin->name() }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                         @endhasrole
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label">Avatar</label>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="{{ asset('back/uploads/avatar.png') }}" class="profile-pic" /> 
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="width: 100%; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn red btn-outline btn-file">
                                                            <span class="fileinput-new btn waves-light btn-primary"> Choisir image </span>
                                                            <span class="fileinput-exists btn waves-light btn-outline-warning"> Changer </span>
                                                            <input type="file" name="avatar" accept="image/png,image/jpg,image/jpeg"> </span>
                                                        <a href="javascript:;" class="btn waves-light btn-danger fileinput-exists" data-dismiss="fileinput"> Supprimer </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-success">NOTE!</span> JPG, PNG seulement. 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Nom
                                                <span class="text-danger"> * </span>
                                            </label>
                                            <input type="text" name="first_name" data-required="1" class="form-control"  value="{{ old('first_name') }}" /> 
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Prénom
                                                <span class="text-danger"> * </span>
                                            </label>
                                            <input type="text" name="last_name" value="{{ old('last_name') }}" data-required="1" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Email
                                                <span class="text-danger"> * </span>
                                            </label>
                                            <input name="email" type="text" value="{{ old('email') }}" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Username</label>
                                            <input name="username" type="text" id="username" value="{{ old('username') }}" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Mot de passe
                                            </label>
                                            <input name="password" type="password" id="password" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Confirmer mot de passe
                                            </label>
                                            <input name="password_confirmation" type="password" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-7"></div>
                                    <div class="col-md-4">
                                        <a href="{{ route('cashiers.index') }}" class="btn btn-outline-info">Annuler</a>
                                        <button type="submit" class="btn waves-effect waves-light btn-success">Ajouter</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection


@section('script')
    <script src="{{ asset('back/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <script src="{{ asset('back/plugins/jquery-validation/js/jquery.validate.js') }}" type="text/javascript"></script>
    <script src="{{ asset('back/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('back/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('back/js/form-validation.js') }}" type="text/javascript"></script>
@endsection