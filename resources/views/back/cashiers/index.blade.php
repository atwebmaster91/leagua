@extends('layouts.back')
@section('title') caissiéres @stop

@section('style')
    <link href="{{ asset('back/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" id="theme" rel="stylesheet">
    <link href="{{ asset('back/plugins/bootstrap-switch/bootstrap-switch.min.css') }}" rel="stylesheet">
    <link href="{{ asset('back/plugins/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
    <link href="{{ asset('back/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Caissiéres</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Tableau de bord</a></li>
                <li class="breadcrumb-item active">Caissiéres</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
            	<a href="{{ route('cashiers.create') }}" class="btn btn-success rounded-0">Nouveau caissiére</a>
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="cashiers-table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%" data-user="{{ Auth::user()->getRoleNames() }}">
                                <thead>
                                    <tr>
                                        <th>Avatar</th>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Parent</th>
                                        <th>Email</th>
                                        <th>Money</th>
                                        <th width="60">Statut</th>
                                        <th width="60">Date de création</th>
                                        <th width="60">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Avatar</th>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Parent</th>
                                        <th>Email</th>
                                        <th>Money</th>
                                        <th>Statut</th>
                                        <th>Date de création</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection


@section('script')
    <script src="{{ asset('back/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('back/plugins/bootstrap-switch/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('back/plugins/toast-master/js/jquery.toast.js') }}"></script>

    <script src="{{ asset('back/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('back/plugins/sweetalert/jquery.sweet-alert.custom.js') }}"></script>
    <script src="{{ asset('back/plugins/datatables/date-euro.js') }}"></script>
    <script src="{{ asset('back/js/data-table.js') }}"></script>
    <script src="{{ asset('back/js/add-script.js') }}"></script>
@endsection