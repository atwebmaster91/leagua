@extends('layouts.connexion')

@section('title'){{ __('Reset Password') }}@endsection

@section('content')
    <div class="login-register">
        <div class="login-box">
            <div class="card-body card">
                <a href="{{ route('login')}}"><img src="{{ asset('back/images/logo.png') }}" width="80" style="display: block;margin: auto; margin-bottom: 20px"></a>
                <form class="form-horizontal form-material" method="POST" action="{{ route('password.update') }}">
                    @csrf
                    <h3 class="box-title m-b-20">Nouvelle Mot de passe</h3>
                    @include('includes.msg')
                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" placeholder="Votre email" name="email" value="{{ old('email') }}" required> 
                            @if ($errors->has('email'))
                                <span class="help-block help-block-error">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" placeholder="Votre mot de passe" name="password" value="{{ old('password') }}" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                        <div class="col-xs-12">
                            <input id="password-confirm" type="password" class="form-control" placeholder="Confirmer votre mot de passe" name="password_confirmation" required>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group text-center m-t-20">
                        <div class="col-md-12">
                            <button class="btn btn-primary" type="submit"> {{ __('Reset Password') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection