@extends('layouts.connexion')

@section('title'){{ __('Login') }}@endsection

@section('content')
    <div class="login-register">
        <div class="login-box">
            <div class="card-body card">
                <a href="{{ route('login')}}"><img src="{{ asset('back/images/logo.png') }}" width="80" style="display: block;margin: auto; margin-bottom: 20px"></a>
                <form class="form-horizontal form-material" id="login" method="post" action="{{ route('login') }}">
                    @csrf
                    <h3 class="box-title m-b-20">Se connecter à votre compte</h3>
                    @include('includes.msg')
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" placeholder="Votre email \ username" name="email" value="{{ old('email') }}" required> 
                            @if ($errors->has('email'))
                                <span class="help-block help-block-error">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" placeholder="Votre mot de passe" name="password" value="{{ old('password') }}" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary pull-left p-t-0">
                                <input type="checkbox" name="remember" id="checkbox-signup" {{ old('remember') ? 'checked' : '' }}>
                                <label for="checkbox-signup">  {{ __('Remember Me') }} </label>
                            </div> 
                            @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> {{ __('Forgot pwd?') }}</a>
                            @endif 
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Valider</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection