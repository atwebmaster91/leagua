<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        @if( Auth::check() ) 
        <div class="user-profile">
            <!-- User profile image -->
            <div class="profile-img"> 
                <img  src="{{ Auth::user()->presentAvatar(true) }}"/>
            </div>
            <!-- User profile text--> 
            <div class="profile-text"> 
                <h5> {{ Auth::user()->name() }}</h5>
                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="mdi mdi-settings"></i></a>
                <a href="{{ route('backstagelogout') }}" class="" data-toggle="tooltip" title="Déconnexion"><i class="mdi mdi-power"></i></a>
                <div class="dropdown-menu animated flipInY">
                <!-- text--> 
                <a href="{{ route('admin.account') }}" class="dropdown-item"><i class="ti-user"></i> Mon compte</a>
                <!-- text--> 
                <div class="dropdown-divider"></div>
                <!-- text-->  
                <a href="{{ route('backstagelogout') }}" class="dropdown-item"><i class="fa fa-power-off"></i> Déconnexion</a>
                <!-- text-->  
                </div>
            </div> 
        </div>
         @endif
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="{{ active_route('admin.dashboard') }}"> 
                    <a class="waves-effect waves-dark {{ active_route('admin.dashboard') }}" href="{{ route('admin.dashboard') }}">
                        <i class="mdi mdi-gauge"></i>
                        <span class="hide-menu">Tableau de bord </span>
                    </a>
                </li>
                @hasrole('super-admin')
                <li class="{{ active_route('admins.*') }}"> 
                    <a class="waves-effect waves-dark {{ active_route('admins.*') }}" href="{{ route('admins.index') }}">
                        <i class="mdi mdi-account-settings-variant"></i>
                        <span class="hide-menu">Admins</span>
                    </a>
                </li>
                @endhasrole
                @hasanyrole('super-admin|admin')
                <li class="{{ active_route('cashiers.*') }}"> 
                    <a class="waves-effect waves-dark {{ active_route('cashiers.*') }}" href="{{ route('cashiers.index') }}">
                        <i class="mdi mdi-account-key"></i>
                        <span class="hide-menu">Caissières</span>
                    </a>
                </li>
                @endhasanyrole
                @hasanyrole('super-admin|admin|cashier')
                <li class="{{ active_route('clients.*') }}"> 
                    <a class="waves-effect waves-dark {{ active_route('clients.*') }}" href="{{ route('clients.index') }}">
                        <i class="mdi mdi-account"></i>
                        <span class="hide-menu">Clients</span>
                    </a>
                </li>
                @endhasanyrole


                <li class="{{ active_route('money.index') }}"> 
                    <a class="waves-effect waves-dark {{ active_route('money.index') }}" href="{{ route('money.index') }}">
                        <i class="mdi mdi-currency-usd"></i>
                        <span class="hide-menu">Versement/Retrait</span>
                    </a>
                </li>

                <li class="{{ active_route('transactions.index') }}"> 
                    <a class="waves-effect waves-dark {{ active_route('transactions.index') }}" href="{{ route('transactions.index') }}">
                        <i class="mdi mdi-swap-horizontal"></i>
                        <span class="hide-menu">Transactions </span>
                    </a>
                </li>
                @hasrole('super-admin')
                <li class="{{ active_route('settings.*') }}"> 
                    <a class="waves-effect waves-dark {{ active_route('settings.*') }}" href="{{ route('settings.index') }}">
                        <i class="mdi mdi-settings"></i>
                        <span class="hide-menu">Settings</span>
                    </a>
                </li>
                @endhasrole
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>