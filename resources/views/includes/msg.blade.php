@if (session('error'))
   <div class="alert alert-danger alertadd">
       {!! session('error') !!}
   </div>
@endif

@if (session('success'))
   <div class="alert alert-success alertadd">
       {!! session('success') !!}
   </div>
@endif

@if(count($errors) > 0)
   <div class="alert alert-danger alertadd">
      @foreach($errors->all() as $error)
         <p>{{ $error }}</p>
      @endforeach
   </div>
@endif

<div class="alert alert-danger hide">
   <button class="close" data-close="alert"></button> Vous avez des erreurs de formulaire. Veuillez vérifier ci-dessous. 
</div>
<div class="alert alert-success hide">
   <button class="close" data-close="alert"></button> La validation de votre formulaire est réussie! 
</div>